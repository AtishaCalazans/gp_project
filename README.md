#GRAYDIYUS
Some info on the different aspects of the game

### Gameplay
The game consists of 3 levels, each with their own unique boss.
You must evade obstacle, terrain, and enemy bullets.
For every enemy and obstacle you hit, you gain points.
Losing a live will cost you points.
If you die, your points get reset to what they were at the beginning of the level

###Multiplayer
The game can be played with 2 players, each controlling their own ship.

###Keybindings
The player can alter the keybindings in the pause menu.
If a key is already associated with an action, it can not be used for another action.
Since swapping of keybindings is not supported, the way to get around this is by
setting a placeholder key, which frees up one of the keys.

Very occasionaly keybindings can be a bit buggy, resetting the bugged out keys in the keybinding menu should fix this.
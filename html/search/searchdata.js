var indexSectionsWithContent =
{
  0: "abcdefghiklmnopqrstuvw~",
  1: "bcdefghklmnopqstvw",
  2: "cdels",
  3: "bcefghklmnopqstvw",
  4: "abcdefghiklmnopqrstuvw~",
  5: "abfhklmnopqrstw",
  6: "abdegs",
  7: "adefghiklmnpqrsu",
  8: "s"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "enums",
  7: "enumvalues",
  8: "related"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Enumerations",
  7: "Enumerator",
  8: "Friends"
};


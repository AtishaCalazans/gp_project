var searchData=
[
  ['singleton_3c_20gamespecs_20_3e',['Singleton&lt; GameSpecs &gt;',['../structsingleton_1_1_game_specs.html#ace75cd68ae013b0c692ec32ef4aef5ea',1,'singleton::GameSpecs']]],
  ['singleton_3c_20highscores_20_3e',['Singleton&lt; Highscores &gt;',['../classsingleton_1_1_highscores.html#a4fd343546b111e5b9eaad0ae63d9d675',1,'singleton::Highscores']]],
  ['singleton_3c_20keybindings_20_3e',['Singleton&lt; Keybindings &gt;',['../classcontroller_1_1_keybindings.html#aeebb28f41658f7462dc8a3b90b69d4f4',1,'controller::Keybindings']]],
  ['singleton_3c_20stopwatch_20_3e',['Singleton&lt; Stopwatch &gt;',['../classsingleton_1_1_stopwatch.html#abc6f85aad8332653a976dc16390a0bdc',1,'singleton::Stopwatch']]],
  ['singleton_3c_20transformation_20_3e',['Singleton&lt; Transformation &gt;',['../classsingleton_1_1_transformation.html#ad02b2174d4bc9cc7404fe0317b80c55d',1,'singleton::Transformation']]]
];

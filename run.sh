#!/bin/bash
mkdir build
currentPath=$(pwd)
goalPath=/build
buildDir="$currentPath$goalPath"
cd $buildDir
cmake $currentPath
make all
cd ../bin
./gradius


#include <iostream>
#include <SFML/Graphics.hpp>
#include "Game.h"
#include "Controller.h"
#include "Model.h"
#include "View.h"

using std::string;

int main() {

    Game game = Game();
    game.run();

    return 0;
}
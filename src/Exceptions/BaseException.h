//
// Created by atisha on 8/23/18.
//

#ifndef GP_PROJECT_FILEEXCEPTION_H
#define GP_PROJECT_FILEEXCEPTION_H

#include <exception>
#include <string>

/**
 * @brief namespace for custom exceptions that are used in this program
 */
namespace exception {

    /**
     * @brief The base for all exceptions in this program
     */
    class BaseException: public std::exception {
    public:

        BaseException() = default;

        /**
         * @brief constructor of BaseException that makes the error string
         * @param message the error message that has to be displayed
         * @param type error type, to be filled in by sub-classes
         */
        BaseException(const std::string& message, const std::string& type) {
            m_msg = type + ": \"" + message + "\"";
        }

        /**
         * @override of the what() function
         */
        const char* what() const noexcept override{
            return m_msg.c_str();
        };

    protected:

        /**
         * @brief description of the error
         */
        std::string m_msg;

    };

    /**
     * @brief forms a base for all file-related exceptions
     */
    class FileException: public BaseException {
    public:

        FileException(const std::string& message, const std::string& type) : BaseException(message, type) {}

    };

    /**
     * @brief this exception is thrown when there was a problem creating/loading a file
     */
    class FileReadingError: public FileException {
    public:

        FileReadingError(const std::string& message) : FileException(message, "FileReadingError") {}

    };

    /**
     * @brief this exception is for values that are read from a file that are not in the required domain
     * e.g. a negative number where there should only be positive numbers
     */
    class DomainError: public FileException {
    public:

        DomainError(const std::string& message) : FileException(message, "DomainError") {}

    };

    /**
     * @brief exception thrown when a texture that is needed by an entity was not previously loaded
     */
    class TextureNotLoadedError: public BaseException {
    public:

        TextureNotLoadedError(const std::string& message) : BaseException(message, "TextureNotLoadedError") {}

    };

    /**
     * @brief exception thrown in switch-case blocks when the default case is reached
     * This is done because all cases in this program are well known so default should in theory never be reached
     */
    class EnumDomainError: public BaseException {
    public:

        EnumDomainError(const std::string& message) : BaseException(message, "EnumDomainError") {}

    };

    class KeyBindingError: public BaseException {
    public:

        KeyBindingError(const std::string& message) : BaseException(message, "KeyBindingError") {}

    };

    }

#endif //GP_PROJECT_FILEEXCEPTION_H

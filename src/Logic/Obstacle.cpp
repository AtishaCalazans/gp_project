//
// Created by atisha on 12.01.18.
//

#include "Obstacle.h"
#include "GameSpecs.h"

namespace logic {

    Obstacle::Obstacle(const float &x, const float &y, const string &texture_name)
    : Entity(x, y, singleton::GameSpecs::get_instance().hitboxes.obstacle){
        m_texture_name = texture_name;
        m_lives = singleton::GameSpecs::get_instance().lives.obstacle;
        m_speed = singleton::GameSpecs::get_instance().speeds.obstacle;
        update_travel_distance_per_frame();
    }

    void Obstacle::move() {
        m_rotation += 0.5;
        move_x(false);
    }

    string Obstacle::get_texture_name() const {
        return m_texture_name;
    }

}
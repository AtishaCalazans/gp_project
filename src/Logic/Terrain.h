//
// Created by atisha on 11.01.18.
//

#ifndef GP_PROJECT_TERRAIN_H
#define GP_PROJECT_TERRAIN_H

#include "Entity.h"

namespace logic {

    /**
     * @brief an entity that the player must avoid hitting
     */
    class Terrain : public Entity{
    public:

        Terrain(const float& x, const float& y, const string& texture_name);

        void move() override ;

        string get_texture_name() const override;

    private:

        string m_texture_name;

    };

}

#endif //GP_PROJECT_TERRAIN_H

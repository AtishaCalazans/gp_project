//
// Created by atisha on 13.12.17.
//

#include "PlayerShip.h"
#include "Stopwatch.h"
#include "GameSpecs.h"

namespace logic {

    PlayerShip::PlayerShip(const float& x, const float& y, const bool first_player)
    : Entity(x, y, singleton::GameSpecs::get_instance().hitboxes.player) {
        m_first_player = first_player;
        m_lives = singleton::GameSpecs::get_instance().lives.player;
        m_speed = singleton::GameSpecs::get_instance().speeds.player;
        update_travel_distance_per_frame();
    }

    void PlayerShip::update_observers() {
        if (m_invulnerability_counter > 0) {
            --m_invulnerability_counter;
            if (m_invulnerability_counter == 0) {m_status = alive;}
        }
        Entity::update_observers();
    }

    string PlayerShip::get_texture_name() const {
        if (m_first_player) {
            return singleton::GameSpecs::get_instance().textures.ship1;
        } else {
            return singleton::GameSpecs::get_instance().textures.ship2;
        }
    }

    void PlayerShip::move_x(const bool& positive) {
        if (positive) {
            m_x += m_move_x_per_frame;
            //if the ship touches the right edge of the screen
            if (m_x + m_hitbox_radius > 4) {m_x = 4 - m_hitbox_radius;}
        }
        else {
            m_x -= m_move_x_per_frame;
            //if the ship touches the left edge of the screen
            if (m_x - m_hitbox_radius < -4) {m_x = -4 + m_hitbox_radius;}
        }
    }

    void PlayerShip::move_y(const bool& positive) {
        if (positive) {
            m_y += m_move_y_per_frame;
            //if the ship touches the top border
            if (m_y + m_hitbox_radius > 2.5) {
                if (m_status != invulnerable) {
                    lose_lives(2);
                }
                m_y = 2.5 - m_hitbox_radius;
            }
        }
        else {
            m_y -= m_move_y_per_frame;
            //if the ship touches the bottom border
            if (m_y - m_hitbox_radius < -2.5) {
                if (m_status != invulnerable) {
                    lose_lives(2);
                }
                m_y = -2.5 + m_hitbox_radius;
            }
        }
    }

    void PlayerShip::move() {}

    void PlayerShip::lose_lives(const int &lives) {
        //only lose lives if not invulnerable
        if (m_status != invulnerable) {
            make_invulnerable();
            Entity::lose_lives(lives);
        }
    }

    void PlayerShip::make_invulnerable() {
        //if the ship is already invulnerable there's no need to make it invulnerable again
        if (m_status != invulnerable) {
            m_status = invulnerable;
            m_invulnerability_counter = singleton::Stopwatch::get_instance().get_fps() * 2;
        }
    }

    int PlayerShip::get_lives() const {
        return m_lives;
    }

}
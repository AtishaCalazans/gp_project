//
// Created by atisha on 15.11.17.
//

#ifndef GP_PROJECT_ENTITY_H
#define GP_PROJECT_ENTITY_H

#include <SFML/Graphics.hpp>
#include <memory>
#include <set>

using std::string;
using std::shared_ptr;
using std::weak_ptr;
using std::set;
namespace draw {class EntityView;}

namespace logic {

    enum Status {alive, dead, invulnerable};

    /**
     * @brief a class that represents a game object
     */
    class Entity{
    public:

        Entity() = default;

        Entity(const float& x, const float& y, const float& radius = 0);

        ~Entity();

        /**
         * @brief add a new observer
         * @param observer the new observer
         */
        void add_observer(const shared_ptr<draw::EntityView>& observer);

        /**
         * @brief update all observers
         */
        virtual void update_observers();

        /**
         * @brief move the entity on the x axis with distance m_move_x_per_frame
         * @param positive true if the movement is positive
         */
        virtual void move_x(const bool& positive);

        /**
         * @brief move the entity on the y axis with distance m_move_y_per_frame
         * @param positive true if the movement is positive
         */
        virtual void move_y(const bool& positive);

        /**
         * @brief pure virtual function that lets the entity move on its own
         */
        virtual void move() = 0;

        /**
         * @return x position
         */
        float get_x() const;

        /**
         * @return y position
         */
        float get_y() const;

        /**
         * @return absolute rotation
         */
        float get_rotation() const;

        /**
         * @brief test if the entity collides with another entity
         * @param other entity that has to be checked for collision
         * @return true if there is a collision
         */
        bool check_collision(const Entity& other); //this function can still be put in playership if the other entities don't need it

        /**
         * @return filename of the texture used
         */
        virtual string get_texture_name() const;

        /**
         * @brief updates the distance needed to travel in one frame
         */
        void update_travel_distance_per_frame();

        /**
         * @brief get the radius of the hitbox
         * @return hitbox radius
         */
        float get_hitbox_radius() const;

        /**
         * @brief get the status of the entity
         * @return status of the entity
         */
        Status get_status() const;

        /**
         * @brief get the visibility of the entity
         * @return a value between 0 and 1
         */
        float get_visibility() const;

        /**
         * @param lives lives that will be subtracted from the lives counter
         */
        virtual void lose_lives(const int& lives);

    protected:

        float m_x;
        float m_y;
        float m_hitbox_radius;
        float m_move_x_per_frame;
        float m_move_y_per_frame;
        float m_speed = 1;
        float m_rotation = 0;
        float m_visibility = 1;
        int m_lives = 1;
        set<shared_ptr<draw::EntityView>> m_observers;
        Status m_status = alive;

    };

}

#endif //GP_PROJECT_ENTITY_H

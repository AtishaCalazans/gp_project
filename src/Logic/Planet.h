//
// Created by atisha on 11.01.18.
//

#ifndef GP_PROJECT_PLANET_H
#define GP_PROJECT_PLANET_H

#include "Loop.h"

namespace logic {

    /**
     * @brief a planet that moves in the background to create a nice atmosphere
     */
    class Planet : public Loop {
    public:

        Planet(const float& x, const float& y, string texture_name);

        string get_texture_name() const override;

    private:

        string m_texture_name;

    };

}

#endif //GP_PROJECT_PLANET_H

//
// Created by atisha on 7/20/18.
//

#ifndef GP_PROJECT_BUTTON_H
#define GP_PROJECT_BUTTON_H

#include "Text.h"

namespace logic {

    enum ButtonStatus {rest, hover, pressed};

    /**
     * @brief a Text class that has extra features so that it can be used as a button
     */
    class Button: public Text {
    public:

        Button(const std::string text, const unsigned int char_size, const float x, const float y);

        /**
         * @param status the new status for the button
         */
        void set_button_status(const ButtonStatus status);

        /**
         * @return the current status of the button
         */
        ButtonStatus get_button_status() const;

    private:

        ButtonStatus m_button_status = rest;

    };

}

#endif //GP_PROJECT_BUTTON_H

//
// Created by atisha on 11.01.18.
//

#include "Loop.h"
#include "GameSpecs.h"

namespace logic {

    Loop::Loop(const float& x, const float& y, const float& speed)
    : Entity(x, y, singleton::GameSpecs::get_instance().hitboxes.loop){
        m_speed = speed;
        update_travel_distance_per_frame();
    }

    void Loop::move() {
        move_x(false);
    }

    void Loop::move_x(const bool& positive) {
        m_x -= m_move_x_per_frame;
        //if entity is off screen (left)
        if (m_x + m_hitbox_radius < -4) {
            //reposition to the right of the screen so the background loops
            m_x += 16;
        }
    }

}
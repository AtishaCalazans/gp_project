//
// Created by atisha on 7/19/18.
//

#include "Text.h"

namespace logic {

    Text::Text(const std::string text, const unsigned int char_size, const float x, const float y) : Entity(x, y) {
        m_text = text;
        m_char_size = char_size;
        m_speed = 0;
    }

    std::string Text::get_text() const {
        return m_text;
    }

    unsigned int Text::get_char_size() const {
        return m_char_size;
    }

    sf::Uint8 Text::get_visibility() const {
        return sf::Uint8(m_visibility);
    }

    void Text::set_text(const std::string& new_text) {
        m_text = new_text;
    }

    void Text::move() {}

}
//
// Created by atisha on 7/20/18.
//

#include "Button.h"

namespace logic {

    Button::Button(const std::string text, const unsigned int char_size, const float x, const float y)
            : Text(text, char_size, x, y) {
        m_button_status = rest;
    }

    void Button::set_button_status(ButtonStatus status) {
        m_button_status = status;
    }

    ButtonStatus Button::get_button_status() const {
        return m_button_status;
    }

}
//
// Created by atisha on 11.01.18.
//

#include "Planet.h"
#include "GameSpecs.h"

namespace logic {

    Planet::Planet(const float& x, const float& y, string texture_name)
            : Loop(x, y, singleton::GameSpecs::get_instance().speeds.planet){
        m_texture_name = texture_name;
        update_travel_distance_per_frame();
    }

    string Planet::get_texture_name() const {
        return m_texture_name;
    }

}
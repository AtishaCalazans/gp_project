//
// Created by atisha on 11.01.18.
//

#include "Terrain.h"
#include "GameSpecs.h"

namespace logic {

    Terrain::Terrain(const float& x, const float& y, const string& texture_name)
    : Entity(x, y, singleton::GameSpecs::get_instance().hitboxes.terrain) {
        m_speed = singleton::GameSpecs::get_instance().speeds.terrain;
        m_texture_name = texture_name;
        update_travel_distance_per_frame();
    }

    void Terrain::move() {
        move_x(false);
    }

    string Terrain::get_texture_name() const {
        return m_texture_name;
    }

}
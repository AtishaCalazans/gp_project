//
// Created by atisha on 26.11.17.
//
#include "Entity.h"
#include "EntityView.h"
#include "Stopwatch.h"
#include <cmath>
#include <iostream>

namespace logic {

    Entity::Entity(const float& x, const float& y, const float& radius) {
        m_x = x;
        m_y = y;
        m_hitbox_radius = radius;
    }

    Entity::~Entity() {}

    void Entity::add_observer(const shared_ptr<draw::EntityView> &observer) {
        m_observers.insert(observer);
    }

    void Entity::update_observers() {
        for (auto& view: m_observers) {
            view->update();
        }
    }

    void Entity::move_x(const bool& positive){
        if (positive) {
            m_x += m_move_x_per_frame;
            //if entity is off screen (right)
            if (m_x - m_hitbox_radius > 4) {
                m_status = dead;
            }
        } else {
            m_x -= m_move_x_per_frame;
            //if entity is off screen (left)
            if (m_x + m_hitbox_radius < -4) {
                m_status = dead;
            }
        }
    }

    void Entity::move_y(const bool& positive){
        if (positive) {
            m_y += m_move_y_per_frame;
            //if entity is off screen (up)
            if (m_y - m_hitbox_radius > 3) {
                m_status = dead;
            }
        } else {
            m_y -= m_move_y_per_frame;
            //if entity is off screen (down)
            if (m_y + m_hitbox_radius < -3) {
                m_status = dead;
            }
        }
    }

    float Entity::get_x() const {
        return m_x;
    }

    float Entity::get_y() const {
        return m_y;
    }

    float Entity::get_rotation() const {
        return m_rotation;
    }

    bool Entity::check_collision(const Entity& other) {
        float combined_radius = m_hitbox_radius + other.m_hitbox_radius;
        float distance_between_centers = std::sqrt(std::pow(m_x - other.m_x, 2) + std::pow(m_y - other.m_y, 2));

        return distance_between_centers < combined_radius;
    }

    string Entity::get_texture_name() const {
        return "dat_boi.png";
    }

    void Entity::update_travel_distance_per_frame() {
        m_move_x_per_frame = m_speed / singleton::Stopwatch::get_instance().get_fps();
        m_move_y_per_frame = m_speed / singleton::Stopwatch::get_instance().get_fps();
    }

    float Entity::get_hitbox_radius() const {
        return m_hitbox_radius;
    }

    Status Entity::get_status() const {
        return m_status;
    }

    float Entity::get_visibility() const {
        return m_visibility;
    }

    void Entity::lose_lives(const int &lives) {
        m_lives -= lives;
        for (auto& observer: m_observers) {
            observer->show_damage();
        }
        if (m_lives < 1) {m_status = dead;}
    }
}
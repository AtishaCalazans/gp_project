//
// Created by atisha on 13.12.17.
//

#ifndef GP_PROJECT_PLAYERSHIP_H
#define GP_PROJECT_PLAYERSHIP_H

#include "Entity.h"

namespace logic {

    /**
     * @brief a class used to create a playership that the user will controll
     */
    class PlayerShip : public Entity {
    public:

        PlayerShip(const float& x, const float& y, const bool first_player);

        void update_observers() override;

        string get_texture_name() const override;

        void move_x(const bool& positive) override;

        void move_y(const bool& positive) override;

        void move() override;

        void lose_lives(const int& lives) override;

        /**
         * @brief makes the ship invulnerable for a brief period of time
         */
        void make_invulnerable();

        /**
         * @return amount of lives
         */
        int get_lives() const;

    private:

        int m_invulnerability_counter = 0;
        bool m_first_player = true;

    };

}

#endif //GP_PROJECT_PLAYERSHIP_H

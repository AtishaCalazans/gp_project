//
// Created by atisha on 7/19/18.
//

#ifndef GP_PROJECT_POINTSTEXT_H
#define GP_PROJECT_POINTSTEXT_H

#include "Text.h"
#include <memory>

namespace logic {

    /**
     * @brief class used to show points gained when an enemy/obstacle is hit
     */
    class PointsText: public Text {
    public:

        /**
         * @brief constructor
         * @param entity used to position the text to the bottom-left corner of the entity
         * @param points_gained the number that has to be displayed
         */
        PointsText(const std::shared_ptr<Entity> entity, const int points_gained);

        void update_observers() override;

    private:

        int m_remaining_lifespan;
        double m_visibility_change_per_frame;

    };

}

#endif //GP_PROJECT_POINTSTEXT_H

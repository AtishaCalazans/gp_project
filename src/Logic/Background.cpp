//
// Created by atisha on 29.12.17.
//

#include "Background.h"
#include "GameSpecs.h"

namespace logic {

    Background::Background(const float& x, const float& y)
            : Loop(x, y, singleton::GameSpecs::get_instance().speeds.background) {}

    string Background::get_texture_name() const {
        return singleton::GameSpecs::get_instance().textures.background;
    }

}
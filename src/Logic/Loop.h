//
// Created by atisha on 11.01.18.
//

#ifndef GP_PROJECT_LOOP_H
#define GP_PROJECT_LOOP_H

#include "Entity.h"

namespace logic {

    /**
     * @brief class used for entities that need to reposition to the right of the screen when they fall off the left
     * of the screen
     */
    class Loop : public Entity {
    public:

        Loop(const float& x, const float& y, const float& speed);

        void move() override;

        void move_x(const bool& positive) override;

    };

}

#endif //GP_PROJECT_LOOP_H

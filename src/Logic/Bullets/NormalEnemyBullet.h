//
// Created by atisha on 13.01.18.
//

#ifndef GP_PROJECT_NORMALENEMYBULLET_H
#define GP_PROJECT_NORMALENEMYBULLET_H

#include "EnemyBullet.h"

namespace logic {

    /**
     * @brief the normal bullet type shot by NormalEnemy
     */
    class NormalEnemyBullet : public EnemyBullet {
    public:

        NormalEnemyBullet(const float& x, const float& y);

        string get_texture_name() const override;

        void move() override;

    };

}

#endif //GP_PROJECT_NORMALENEMYBULLET_H

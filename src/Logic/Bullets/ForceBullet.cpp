//
// Created by atisha on 8/21/18.
//
#include "ForceBullet.h"
#include "GameSpecs.h"

namespace logic {

    ForceBullet::ForceBullet(const float &x, const float &y, logic::direction dir)
    : EnemyBullet(x, y, singleton::GameSpecs::get_instance().hitboxes.force_bullet) {
        m_dir = dir;
        m_speed = singleton::GameSpecs::get_instance().speeds.force_bullet;
        update_travel_distance_per_frame();
    }

    string ForceBullet::get_texture_name() const {
        return singleton::GameSpecs::get_instance().textures.force_bullet;
    }

    void ForceBullet::move() {
        move_x(false);
        if (m_dir == up) {
            m_y -= m_move_y_per_frame / 4;
            //if entity is off screen (down)
            if (m_y + m_hitbox_radius < -3) {
                m_status = dead;
            }
        } else if (m_dir == down) {
            m_y += m_move_y_per_frame / 4;
            //if entity is off screen (up)
            if (m_y - m_hitbox_radius > 3) {
                m_status = dead;
            }
        }
    }

}
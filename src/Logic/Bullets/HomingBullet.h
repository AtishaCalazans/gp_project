//
// Created by atisha on 13.01.18.
//

#ifndef GP_PROJECT_HOMINGBULLET_H
#define GP_PROJECT_HOMINGBULLET_H

#include "EnemyBullet.h"

namespace logic {

    class PlayerShip;

    /**
     * @brief bullet that aims at the player
     */
    class HomingBullet : public EnemyBullet{
    public:

        /**
         * @param ship pointer to the playership so the bullet can direct itself towards the ship
         */
        HomingBullet(const float& x, const float& y, weak_ptr<PlayerShip>& ship);

        string get_texture_name() const override;

        void move() override;

    private:

        //to keep track of where the player is
        weak_ptr<PlayerShip> m_ship;

    };

}

#endif //GP_PROJECT_HOMINGBULLET_H

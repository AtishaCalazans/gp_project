//
// Created by atisha on 13.01.18.
//

#ifndef GP_PROJECT_ENEMYBULLET_H
#define GP_PROJECT_ENEMYBULLET_H

#include "Entity.h"

namespace logic {

    /**
     * @brief an enemy bullet that will damage the playership
     */
    class EnemyBullet : public Entity {
    public:

        EnemyBullet(const float& x, const float& y, const float& radius) : Entity(x, y, radius){};

    };

}

#endif //GP_PROJECT_ENEMYBULLET_H

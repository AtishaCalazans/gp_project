//
// Created by atisha on 8/21/18.
//

#ifndef GP_PROJECT_FORCEBULLET_H
#define GP_PROJECT_FORCEBULLET_H

#include "EnemyBullet.h"

namespace logic {

    enum direction {
        up,
        middle,
        down
    };

    /**
     * @brief bullet class for ForceEnemy
     */
    class ForceBullet: public EnemyBullet{
    public:

        ForceBullet(const float& x, const float& y, direction dir);

        string get_texture_name() const override;

        void move() override;

    private:


        direction m_dir;
    };

}

#endif //GP_PROJECT_FORCEBULLET_H

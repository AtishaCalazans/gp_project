//
// Created by atisha on 8/8/18.
//
#include "LaserBullet.h"
#include "GameSpecs.h"

namespace logic {

    LaserBullet::LaserBullet(const float &x, const float &y)
    : EnemyBullet(x, y, singleton::GameSpecs::get_instance().hitboxes.laser_bullet) {
        m_speed = singleton::GameSpecs::get_instance().speeds.laser_bullet;
        update_travel_distance_per_frame();
    }

    string LaserBullet::get_texture_name() const {
        return singleton::GameSpecs::get_instance().textures.laser_bullet;
    }

    void LaserBullet::move() {
        move_x(false);
    }

}

//
// Created by atisha on 13.01.18.
//

#include <Stopwatch.h>
#include "NormalEnemyBullet.h"
#include "GameSpecs.h"

namespace logic {

    NormalEnemyBullet::NormalEnemyBullet(const float& x, const float& y)
    : EnemyBullet(x, y, singleton::GameSpecs::get_instance().hitboxes.normal_enemy_bullet) {
        m_speed = singleton::GameSpecs::get_instance().speeds.normal_enemy_bullet;
        update_travel_distance_per_frame();
        m_move_y_per_frame = 2 / singleton::Stopwatch::get_instance().get_fps();
    }

    string NormalEnemyBullet::get_texture_name() const {
        return singleton::GameSpecs::get_instance().textures.normal_enemy_bullet;
    }

    void NormalEnemyBullet::move() {
        move_x(false);
    }

}
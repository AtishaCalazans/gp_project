//
// Created by atisha on 29.12.17.
//

#ifndef GP_PROJECT_BULLET_H
#define GP_PROJECT_BULLET_H

#include "Entity.h"

namespace logic {

    /**
     * @brief bullet type that is shot by the player
     */
    class FriendlyBullet : public Entity {
    public:

        FriendlyBullet(const float& x, const float& y);

        string get_texture_name() const override;

        void move() override;

    };

}

#endif //GP_PROJECT_BULLET_H

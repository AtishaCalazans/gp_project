//
// Created by atisha on 8/8/18.
//

#ifndef GP_PROJECT_LASERBULLET_H
#define GP_PROJECT_LASERBULLET_H

#include "EnemyBullet.h"

namespace logic {

    /**
     * @brief bullet for LaserEnemy
     */
    class LaserBullet: public EnemyBullet {
    public:

        LaserBullet(const float& x, const float& y);

        string get_texture_name() const override;

        void move() override;
    };


}

#endif //GP_PROJECT_LASERBULLET_H

//
// Created by atisha on 13.01.18.
//

#include <Stopwatch.h>
#include <PlayerShip.h>
#include "HomingBullet.h"
#include "GameSpecs.h"

namespace logic {

    HomingBullet::HomingBullet(const float& x, const float& y, weak_ptr<PlayerShip>& ship)
    : EnemyBullet(x, y, singleton::GameSpecs::get_instance().hitboxes.homing_bullet) {
        m_ship = ship;
        m_speed = singleton::GameSpecs::get_instance().speeds.homing_bullet;
        update_travel_distance_per_frame();
        m_move_y_per_frame = 1.0 / singleton::Stopwatch::get_instance().get_fps();
    }

    string HomingBullet::get_texture_name() const {
        return singleton::GameSpecs::get_instance().textures.homing_bullet;
    }

    void HomingBullet::move() {
        move_x(false);
        shared_ptr<PlayerShip> ship = m_ship.lock();

        //if the bullet is to the right of the ship, aim for the ship
        if (m_x > ship->get_x()) {
            //if the bullet is higher than the ship move down else do the opposite
            if (m_y > ship->get_y()) {move_y(false);}
            else if (m_y < ship->get_y()) {move_y(true);}
        }
    }

}
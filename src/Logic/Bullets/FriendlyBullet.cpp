//
// Created by atisha on 29.12.17.
//

#include "FriendlyBullet.h"
#include "Stopwatch.h"
#include "GameSpecs.h"
#include <iostream>

namespace logic {

    FriendlyBullet::FriendlyBullet(const float& x, const float& y)
    : Entity(x, y, singleton::GameSpecs::get_instance().hitboxes.friendly_bullet) {
        m_speed = singleton::GameSpecs::get_instance().speeds.friendly_bullet;
        update_travel_distance_per_frame();
    }

    string FriendlyBullet::get_texture_name() const {
        return singleton::GameSpecs::get_instance().textures.friendly_bullet;
    }

    void FriendlyBullet::move() {
        move_x(true);
    }

}
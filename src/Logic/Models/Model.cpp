//
// Created by atisha on 09.12.17.
//

#include "Model.h"
#include "View.h"
#include "PlayerShip.h"
#include "Bullets/FriendlyBullet.h"
#include "Bullets/NormalEnemyBullet.h"
#include "Bullets/HomingBullet.h"
#include "World.h"
#include "Background.h"
#include "Planet.h"
#include "Terrain.h"
#include "Obstacle.h"
#include "Text.h"
#include "Button.h"
#include "PointsText.h"
#include "Enemies/Enemy.h"

namespace logic {

    Model::Model(shared_ptr<draw::View> view) {
        m_view = view;
    }

    void Model::tick() {
        //update positions and status of entity
        for (auto& entity: m_entities) {
            entity->update_observers();
        }

        delete_dead_entities();

        //draw all entities
        m_view->draw();
    }

    void Model::key_string_pressed(const string &key_str) {}

    void Model::switch_debug() {
        m_view->switch_debug();
    }

    void Model::set_multiplayer(const bool multiplayer){
        m_multiplayer = multiplayer;
    }

    void Model::delete_dead_entities() {
        //delete all dead entities
        auto entity_it = m_entities.begin();

        while (entity_it != m_entities.end()) {
            if ((*entity_it)->get_status() == dead) {
                (*entity_it).reset();
                m_entities.erase(entity_it++);
            } else {
                ++entity_it;
            }
        }
    }

    void Model::flush(bool hard_flush) {
        m_entities.clear();
        m_view->flush(hard_flush);
    }

    game_status Model::get_game_status() {return m_game_status;}

    void Model::key_pressed(const sf::Keyboard::Key) {}

}
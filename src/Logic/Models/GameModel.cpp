//
// Created by atisha on 09.12.17.
//

#include "GameModel.h"
#include "View.h"
#include "Stopwatch.h"
#include "GameSpecs.h"
#include "Highscores.h"
#include "PlayerShip.h"
#include "Bullets/FriendlyBullet.h"
#include "Bullets/NormalEnemyBullet.h"
#include "Bullets/HomingBullet.h"
#include "Bullets/LaserBullet.h"
#include "Enemies/ForceField.h"
#include "Bullets/ForceBullet.h"
#include "World.h"
#include "Background.h"
#include "Planet.h"
#include "Terrain.h"
#include "Obstacle.h"
#include "Text.h"
#include "Button.h"
#include "PointsText.h"
#include "Enemies/Enemy.h"
#include "BaseException.h"

namespace logic {

    GameModel::GameModel(shared_ptr<draw::View> view) : Model(view) {
        m_game_status = start_singleplayer;
        //let bullets fire at a rate of 5 per second
        m_shoot_buffer_max = singleton::Stopwatch::get_instance().get_fps() / singleton::GameSpecs::get_instance().bullet_rates.player;
    }

    GameModel::~GameModel() {
        m_view->flush(true);
    }

    template<class T>
    shared_ptr<T> GameModel::create_entity(const float &x, const float &y) {
        shared_ptr<T> new_entity = std::make_shared<T>(x, y);
        m_entities.push_back(new_entity);
        m_view->add_entity(new_entity);
        return new_entity;
    }

    void GameModel::tick() {
        //decrease the shooting buffers
        if (m_shoot_buffer_1 > 0) {
            m_shoot_buffer_1 -= 1;
        }
        if (m_shoot_buffer_2 > 0) {
            m_shoot_buffer_2 -= 1;
        }

        //check if any enemies are firing bullets
        for (auto &enemy: m_enemies) {
            bullet_type bullet = enemy->is_shooting();
            if (bullet != none) create_enemy_bullet(enemy->get_x(), enemy->get_y(), bullet);
        }

        //move all entities
        for (auto &entity: m_entities) {
            entity->move();
        }

        check_collisions();
        //update live counter
        if (!m_multiplayer) m_lives->set_text("Lives: " + std::to_string(m_ship_1->get_lives()));
        else
            m_lives->set_text(
                    "P1: " + std::to_string(m_ship_1->get_lives()) + " P2: " + std::to_string(m_ship_2->get_lives()));

        //update positions and status of entity
        for (auto &entity: m_entities) {
            entity->update_observers();
        }

        delete_dead_entities();

        //draw all entities
        m_view->draw();
    }

    void GameModel::action(const bool player_one) {
        if (player_one and m_shoot_buffer_1 == 0) {
            m_friendly_bullets.push_back(create_entity<FriendlyBullet>(m_ship_1->get_x(), m_ship_1->get_y()));
            m_shoot_buffer_1 = m_shoot_buffer_max;
        } else if (!player_one and m_multiplayer and m_shoot_buffer_2 == 0) {
            m_friendly_bullets.push_back(create_entity<FriendlyBullet>(m_ship_2->get_x(), m_ship_2->get_y()));
            m_shoot_buffer_2 = m_shoot_buffer_max;
        }
    }

    void GameModel::create_enemy_bullet(const float &x, const float &y, bullet_type type) {
        switch (type) {
            case homing_b: {
                weak_ptr<PlayerShip> weak_ship_pointer = m_ship_1;
                auto homing_bullet = std::make_shared<HomingBullet>(x, y, weak_ship_pointer);
                m_entities.push_back(homing_bullet);
                m_enemy_bullets.push_back(homing_bullet);
                m_view->add_entity(homing_bullet);
                break;
            }
            case normal_b:
                m_enemy_bullets.push_back(create_entity<NormalEnemyBullet>(x, y));
                break;
            case laser_b:
                m_enemy_bullets.emplace_back(create_entity<LaserBullet>(x, y));
                break;
            case force_b: {
                auto force_bullet_1 = std::make_shared<ForceBullet>(x, y, up);
                auto force_bullet_2 = std::make_shared<ForceBullet>(x, y, middle);
                auto force_bullet_3 = std::make_shared<ForceBullet>(x, y, down);
                m_entities.push_back(force_bullet_1);
                m_enemy_bullets.push_back(force_bullet_1);
                m_view->add_entity(force_bullet_1);
                m_entities.push_back(force_bullet_2);
                m_enemy_bullets.push_back(force_bullet_2);
                m_view->add_entity(force_bullet_2);
                m_entities.push_back(force_bullet_3);
                m_enemy_bullets.push_back(force_bullet_3);
                m_view->add_entity(force_bullet_3);
                break;
            }
            default:
                break;
        }

    }

    void GameModel::create_terrain(const int &height, const int &length, const bool &down) {
        double start_x = 4.25;
        double start_y = down ? 2.25f : -2.25f;

        for (int i = 0; i < height; ++i) {
            double current_y = down ? start_y - (0.5 * i) : start_y + (0.5 * i);
            for (int j = 0; j < length; ++j) {
                double current_x = start_x + j;

                //always make 2 blocks
                auto terrain_1 = std::make_shared<Terrain>(current_x, current_y, m_terrain_name);
                m_entities.push_back(terrain_1);
                m_view->add_background(terrain_1);
                m_terrain.push_back(terrain_1);

                auto terrain_2 = std::make_shared<Terrain>(current_x + 0.5, current_y, m_terrain_name);
                m_entities.push_back(terrain_2);
                m_view->add_background(terrain_2);
                m_terrain.push_back(terrain_2);
            }
        }
    }

    void GameModel::create_obstacle(const float &y) {
        std::shared_ptr<Obstacle> new_obstacle = std::make_shared<Obstacle>(5, y, m_obstacle_name);
        m_entities.push_back(new_obstacle);
        m_view->add_entity(new_obstacle);
        m_obstacles.push_back(new_obstacle);
    }

    void GameModel::create_enemy(enemy_type type, const float &height) {
        shared_ptr<Enemy> new_enemy = EnemyFactory::get_enemy(5, height, type);
        m_enemies.push_back(new_enemy);
        m_entities.push_back(new_enemy);
        m_view->add_entity(new_enemy);
        if (type == force) {
            m_enemies.emplace_back(create_entity<ForceField>(5, height));
        }
    }

    void GameModel::move_down(const bool player_one) {
        if (player_one) {
            m_ship_1->move_y(true);
        } else if (m_multiplayer) {
            m_ship_2->move_y(true);
        }
    }

    void GameModel::move_up(const bool player_one) {
        if (player_one) {
            m_ship_1->move_y(false);
        } else if (m_multiplayer) {
            m_ship_2->move_y(false);
        }
    }

    void GameModel::move_right(const bool player_one) {
        if (player_one) {
            m_ship_1->move_x(true);
        } else if (m_multiplayer) {
            m_ship_2->move_x(true);
        }
    }

    void GameModel::move_left(const bool player_one) {
        if (player_one) {
            m_ship_1->move_x(false);
        } else if (m_multiplayer) {
            m_ship_2->move_x(false);
        }
    }

    void GameModel::load_level(json &level) {
        json textures = level["Textures"];
        vector<string> texture_names;
        //these are all the textures that are needed in each level;
        texture_names.emplace_back(singleton::GameSpecs::get_instance().textures.ship1);
        texture_names.emplace_back(singleton::GameSpecs::get_instance().textures.ship2);
        texture_names.emplace_back(singleton::GameSpecs::get_instance().textures.friendly_bullet);
        texture_names.emplace_back(singleton::GameSpecs::get_instance().textures.background);

        int terrain = textures["Terrain"];
        switch (terrain) {
            case 0:
                m_terrain_name = singleton::GameSpecs::get_instance().textures.terrain1;
                break;
            case 1:
                m_terrain_name = singleton::GameSpecs::get_instance().textures.terrain2;
                break;
            case 2:
                m_terrain_name = singleton::GameSpecs::get_instance().textures.terrain3;
                break;
            default:
                throw exception::DomainError("\"Terrain\" should be a value from 0 to 2");
        }
        texture_names.emplace_back(m_terrain_name);

        int obstacle = textures["Obstacle"];
        switch (obstacle) {
            case 0:
                m_obstacle_name = singleton::GameSpecs::get_instance().textures.obstacle1;
                break;
            case 1:
                m_obstacle_name = singleton::GameSpecs::get_instance().textures.obstacle2;
                break;
            case 2:
                m_obstacle_name = singleton::GameSpecs::get_instance().textures.obstacle3;
                break;
            default:
                throw exception::DomainError("\"Obstacle\" should be a value from 0 to 2");
        }
        texture_names.emplace_back(m_obstacle_name);

        int planet = textures["Planet"];
        switch (planet) {
            case 0:
                m_planet_name = singleton::GameSpecs::get_instance().textures.planet1;
                break;
            case 1:
                m_planet_name = singleton::GameSpecs::get_instance().textures.planet2;
                break;
            case 2:
                m_planet_name = singleton::GameSpecs::get_instance().textures.planet3;
                break;
            default:
                throw exception::DomainError("\"Planet\" should be a value from 0 to 2");
        }
        texture_names.emplace_back(m_planet_name);

        json enemies = textures["Enemies"];
        for (int enemy: enemies) {
            switch (enemy) {
                case 0:
                    texture_names.emplace_back(singleton::GameSpecs::get_instance().textures.normal_enemy);
                    break;
                case 1:
                    texture_names.emplace_back(singleton::GameSpecs::get_instance().textures.quick_enemy);
                    break;
                case 2:
                    texture_names.emplace_back(singleton::GameSpecs::get_instance().textures.laser_enemy);
                    break;
                case 3:
                    texture_names.emplace_back(singleton::GameSpecs::get_instance().textures.homing_enemy);
                    break;
                case 4:
                    texture_names.emplace_back(singleton::GameSpecs::get_instance().textures.force_enemy);
                    break;
                case 5:
                    texture_names.emplace_back(singleton::GameSpecs::get_instance().textures.force_field);
                    break;
                default:
                    throw exception::DomainError("\"Enemies\" should be a value from 0 to 5");
            }
        }

        json enemy_bullets = textures["Enemy_bullets"];
        for (int bullet: enemy_bullets) {
            switch (bullet) {
                case 0:
                    texture_names.emplace_back(singleton::GameSpecs::get_instance().textures.normal_enemy_bullet);
                    break;
                case 1:
                    texture_names.emplace_back(singleton::GameSpecs::get_instance().textures.laser_bullet);
                    break;
                case 2:
                    texture_names.emplace_back(singleton::GameSpecs::get_instance().textures.homing_bullet);
                    break;
                case 3:
                    texture_names.emplace_back(singleton::GameSpecs::get_instance().textures.force_bullet);
                    break;
                default:
                    throw exception::DomainError("\"Enemy_bullets\" should be a value from 0 to 3");
            }
        }

        m_view->load_textures(texture_names);

        initialize_entities();
    }

    void GameModel::initialize_entities() {
        m_game_status = playing;

        //add background 1
        auto background_1 = std::make_shared<Background>(0, 0);
        m_entities.push_back(background_1);
        m_view->add_background(background_1, 8, 6);
        //add background 2
        auto background_2 = std::make_shared<Background>(8, 0);
        m_entities.push_back(background_2);
        m_view->add_background(background_2, 8, 6);

        //add bottom terrain 1
        auto terrain_bottom = std::make_shared<World>(0, 2.75, m_terrain_name);
        m_entities.push_back(terrain_bottom);
        m_view->add_background(terrain_bottom, 8, 0);
        //add bottom terrain 1
        auto terrain_top = std::make_shared<World>(0, -2.75, m_terrain_name);
        m_entities.push_back(terrain_top);
        m_view->add_background(terrain_top, 8, 0);

        //add bottom terrain 2
        auto terrain_bottom_2 = std::make_shared<World>(8, 2.75, m_terrain_name);
        m_entities.push_back(terrain_bottom_2);
        m_view->add_background(terrain_bottom_2, 8, 0);
        //add bottom terrain 2
        auto terrain_top_2 = std::make_shared<World>(8, -2.75, m_terrain_name);
        m_entities.push_back(terrain_top_2);
        m_view->add_background(terrain_top_2, 8, 0);

        //add planet
        auto planet = std::make_shared<Planet>(8, -1, m_planet_name);
        m_entities.push_back(planet);
        m_view->add_background(planet, 0, 0);

        if (m_multiplayer) {
            m_ship_1 = std::make_shared<PlayerShip>(-3, -2, true);
            m_entities.push_back(m_ship_1);
            m_view->add_entity(m_ship_1);
            m_ship_2 = std::make_shared<PlayerShip>(-3, 2, false);
            m_entities.push_back(m_ship_2);
            m_view->add_entity(m_ship_2);
        } else {
            m_ship_1 = std::make_shared<PlayerShip>(-3, 0, true);
            m_entities.push_back(m_ship_1);
            m_view->add_entity(m_ship_1);
        }

        //add info_text
        std::string lives_text;
        if (!m_multiplayer) {
            lives_text = "Lives: " + std::to_string(m_ship_1->get_lives());
            m_lives = std::make_shared<Text>(lives_text, 20, -3.2, -2.6);
        } else {
            lives_text =
                    "P1: " + std::to_string(m_ship_1->get_lives()) + " P2: " + std::to_string(m_ship_2->get_lives());
            m_lives = std::make_shared<Text>(lives_text, 20, -2.7, -2.6);
        }
        m_entities.push_back(m_lives);
        m_text.push_back(m_lives);
        m_view->add_text(m_lives);

        //add score text
        string score_text = "Score: " + std::to_string(singleton::Highscores::get_instance().m_current_score);
        m_score = std::make_shared<Text>(score_text, 20, 3.2, -2.6);
        m_entities.push_back(m_score);
        m_text.push_back(m_score);
        m_view->add_text(m_score);
    }

    void GameModel::delete_dead_entities() {
        if (m_ship_1->get_status() == dead) {
            m_game_status = game_over;
            singleton::Highscores::get_instance().fallback();
        } else if (m_multiplayer) {
            if (m_ship_2->get_status() == dead) {
                m_game_status = game_over;
                singleton::Highscores::get_instance().fallback();
            }
        }

        //remove all dead bullets from m_friendly_bullets
        auto bullet_it = m_friendly_bullets.begin();

        while (bullet_it != m_friendly_bullets.end()) {
            if ((*bullet_it)->get_status() == dead) {
                m_friendly_bullets.erase(bullet_it++);
            } else {
                ++bullet_it;
            }
        }

        //remove all dead bullets from m_enemy_bullets
        auto en_bullet_it = m_enemy_bullets.begin();

        while (en_bullet_it != m_enemy_bullets.end()) {
            if ((*en_bullet_it)->get_status() == dead) {
                m_enemy_bullets.erase(en_bullet_it++);
            } else {
                ++en_bullet_it;
            }
        }

        //remove all dead enemies from m_enemies
        auto enemy_it = m_enemies.begin();

        while (enemy_it != m_enemies.end()) {
            if ((*enemy_it)->get_status() == dead) {
                m_enemies.erase(enemy_it++);
            } else {
                ++enemy_it;
            }
        }

        //remove all dead obstacles from m_obstacles
        auto obstacle_it = m_obstacles.begin();

        while (obstacle_it != m_obstacles.end()) {
            if ((*obstacle_it)->get_status() == dead) {
                m_obstacles.erase(obstacle_it++);
            } else {
                ++obstacle_it;
            }
        }

        //remove all dead terrain from m_terrain
        auto terrain_it = m_terrain.begin();

        while (terrain_it != m_terrain.end()) {
            if ((*terrain_it)->get_status() == dead) {
                m_terrain.erase(terrain_it++);
            } else {
                ++terrain_it;
            }
        }

        //remove all dead text from m_text
        auto text_it = m_text.begin();

        while (text_it != m_text.end()) {
            if ((*text_it)->get_status() == dead) {
                m_text.erase(text_it++);
            } else {
                ++text_it;
            }
        }

        Model::delete_dead_entities();
    }

    void GameModel::check_collisions() {
        //playership collisions
        GameModel::check_ship_collisions(m_ship_1);
        if (m_multiplayer) { GameModel::check_ship_collisions(m_ship_2); }


        //collision bullets with enemies/obstacles
        for (auto &bullet: m_friendly_bullets) {
            for (auto &enemy: m_enemies) {
                if (bullet->check_collision(*enemy)) {
                    enemy->lose_lives(1);
                    bullet->lose_lives(1);

                    //add point text
                    add_points(30, enemy);
                }
            }

            for (auto &obstacle: m_obstacles) {
                if (bullet->check_collision(*obstacle)) {
                    obstacle->lose_lives(1);
                    bullet->lose_lives(1);

                    //add point text
                    add_points(20, obstacle);
                }
            }
        }
    }

    void GameModel::check_ship_collisions(const shared_ptr<logic::PlayerShip> &ship) {
        //collision with world border is handled inside of playership

        if (ship->get_status() != invulnerable) {
            //collision with terrain
            for (auto &terrain_block: m_terrain) {
                if (ship->check_collision(*terrain_block)) {
                    ship->lose_lives(2);
                    add_points(-400);

                }
            }

            //collision with obstacles
            for (auto &obstacle: m_obstacles) {
                if (ship->check_collision(*obstacle)) {
                    ship->lose_lives(1);
                    add_points(-200);
                }
            }

            //collision with enemy bullets
            for (auto &bullet: m_enemy_bullets) {
                if (ship->check_collision(*bullet)) {
                    ship->lose_lives(1);
                    bullet->lose_lives(1);
                    add_points(-200);
                }
            }

            //collision with enemies
            for (auto &enemy: m_enemies) {
                if (ship->check_collision(*enemy)) {
                    ship->lose_lives(1);
                    add_points(-200);
                }
            }
        }
    }

    void GameModel::add_points(const int &points, const std::shared_ptr<Entity>& entity) {
        //update score
        singleton::Highscores::get_instance().add_to_score(points);
        //update score string
        string score_text = "Score: " + std::to_string(singleton::Highscores::get_instance().m_current_score);
        m_score->set_text(score_text);

        if (entity != nullptr) {
            //add point text
            auto points_text = std::make_shared<PointsText>(entity, 30);
            m_entities.push_back(points_text);
            m_text.push_back(points_text);
            m_view->add_text(points_text);
        }
    }

    void GameModel::flush(const bool hard_flush) {
        m_friendly_bullets.clear();
        m_enemy_bullets.clear();
        m_terrain.clear();
        m_obstacles.clear();
        m_enemies.clear();
        m_text.clear();
        m_ship_1.reset();
        m_ship_2.reset();
        Model::flush(hard_flush);
    }

    void GameModel::end_level() {
        if (m_enemies.empty()) {
            m_game_status = next_level;
            singleton::Highscores::get_instance().lock_fallback_score();
        }
    }

    void GameModel::switch_pause(bool pause_game) {
        if (pause_game) {
            m_game_status = game_status::pause;
        } else {
            m_game_status = game_status::playing;
        }
    }

}
//
// Created by atisha on 8/22/18.
//

#ifndef GP_PROJECT_HIGHSCOREMODEL_H
#define GP_PROJECT_HIGHSCOREMODEL_H

#include "Model.h"

namespace logic {

    /**
     * @brief Inherited class from Model that handles highscores
     */
    class HighscoreModel: public Model {
    public:

        HighscoreModel(shared_ptr<draw::View>& view);

        ~HighscoreModel();

        /**
         * @brief make all calculations for one frame and notify the view
         */
        void tick() override;

        /**
         * @brief action key pressed
         * @param player_one true if action is action_1
         */
        void action(const bool player_one) override;

        /**
         * @brief give the model the string representation of the key being pressed
         */
        void key_string_pressed(const string& key_str) override;

        /**
         * @brief initialize all entities in this model
         */
        void initialize_entities() override;

    private:

        bool m_input = false; //true if the model is receiving input
        int m_current_amount_of_characters = 0;
        shared_ptr<Text> m_input_text;
        int m_wait_buffer;

        /**
         * @brief initialize the entities for the input screen
         */
        void initialize_input_screen();

        /**
         * @brief initialize the entities for the highscore screen
         */
        void initialize_highscore_screen();

    };

}

#endif //GP_PROJECT_HIGHSCOREMODEL_H

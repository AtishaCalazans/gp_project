//
// Created by atisha on 7/21/18.
//

#ifndef GP_PROJECT_MENUMODEL_H
#define GP_PROJECT_MENUMODEL_H

#include "Model.h"

namespace logic {

    class Text;
    class Button;

    class MenuModel: public Model {
    public:

        MenuModel(shared_ptr<draw::View> view);

        ~MenuModel();

        void tick();

        void move_up(const bool player_one);

        void move_down(const bool player_one);

        void move_right(const bool player_one);

        void move_left(const bool player_one);

        void action(const bool player_one);

        void initialize_entities();

        void delete_dead_entities();

    private:

        enum selected_button {
        singleplayer_b,
        multiplayer_b,
        highscores_b,
        exit_b
    };

        list<std::shared_ptr<Text>> m_text;
        selected_button m_current_button = singleplayer_b;
        int m_select_buffer = 0;
        int m_select_buffer_max;
        int m_wait_buffer;

    };

}

#endif //GP_PROJECT_MENUMODEL_H

//
// Created by atisha on 7/20/18.
//

#ifndef GP_PROJECT_GAMEMODEL_H
#define GP_PROJECT_GAMEMODEL_H

#include "../json.hpp"
#include "Enemies/EnemyFactory.h"
#include "Enemies/Enemy.h"
#include "Model.h"
#include <vector>
#include <memory>
#include <list>

using std::vector;
using std::shared_ptr;
using std::list;
using nlohmann::json;
namespace draw {class View;}

namespace logic {

    class Entity;
    class PlayerShip;
    class FriendlyBullet;
    class EnemyBullet;
    class Loop;
    class Terrain;
    class Obstacle;
    class Text;
    class Button;

    /**
     * @brief class that contains all entities and communicates with View to display these entities
     */
    class GameModel : public Model{
    public:

        GameModel(shared_ptr<draw::View> view);

        ~GameModel();

        /**
         * @brief creates an entity of type T and returns a shared_ptr to that entity (can only be used for entities
         * that only require x and y in the constructor)
         * @tparam T type of the entity that has to be created
         * @param x x position
         * @param y y position
         * @return shared_ptr of entity that was created
         */
        //looks ugly
        template <class T>
        shared_ptr<T> create_entity(const float&x, const float&y);

        /**
         * @brief make all calculations for one frame and notify the view
         */
        void tick();

        /**
         * @brief move the spaceship up
         */
        void move_up(const bool player_one);

        /**
         * @brief move the spaceship down
         */
        void move_down(const bool player_one);

        /**
         * @brief move the spaceship to the right
         */
        void move_right(const bool player_one);

        /**
         * @brief move the spaceship to the left
         */
        void move_left(const bool player_one);

        /**
         * @brief create a bullet entity
         */
        void action(const bool player_one);

        /**
         * @brief create an enemy bullet
         * @param x x position
         * @param y y position
         * @param homing true if the bullet is a homing bullet
         */
        void create_enemy_bullet(const float& x, const float& y, bullet_type type);

        /**
         * @brief create a batch of terrain entities
         * @param height amount of terrain entities in the y direction
         * @param length amount of terrain entities in the x direction
         * @param down true -> terrain is at the bottom, false -> terrain is at the top
         */
        void create_terrain(const int& height, const int& length, const bool& down);

        /**
         * @brief spawn an obstacle to the right of the screen
         * @param y the height of the obstacle
         */
        void create_obstacle(const float& y);

        /**
         * @brief create an enemy to the right of the screen
         * @param type enemy type
         * @param height height at which the enemy spawns
         */
        void create_enemy(enemy_type type, const float& height);

        /**
         * @brief load textures from a json object
         * @param level json object from the nlohmann library
         */
        void load_level(json& level);

        /**
         * @brief creates backgrounds + ship and places them in the right position
         */
        void initialize_entities();

        /**
         * @brief deletes all dead entities
         */
        void delete_dead_entities();

        /**
         * @brief check for collisions
         */
        void check_collisions();

        /**
         * @brief checks collisions against ships
         * @param ship ship to be checked
         */
        void check_ship_collisions(const shared_ptr<PlayerShip>& ship);

        /**
         * @brief add points to the current score
         */
        void add_points(const int &points, const std::shared_ptr<Entity>& entity=nullptr);

        /**
         * @brief delete all entities
         * @param hard_flush true if textures have to be removed as well
         */
        void flush(const bool hard_flush);

        /**
         * @brief tries to set the game_status to 'next_level', this won't happen if there are still enemies alive
         */
        void end_level();

        /**
         * @brief pause or unpause the game
         * @param pause_game
         */
        void switch_pause(bool pause_game);

    private:

        list<shared_ptr<FriendlyBullet>> m_friendly_bullets;
        list<shared_ptr<EnemyBullet>> m_enemy_bullets;
        list<shared_ptr<Terrain>> m_terrain;
        list<shared_ptr<Obstacle>> m_obstacles;
        list<shared_ptr<Enemy>> m_enemies;
        shared_ptr<PlayerShip> m_ship_1;
        shared_ptr<PlayerShip> m_ship_2;
        shared_ptr<Text> m_lives;
        shared_ptr<Text> m_score;
        std::string m_terrain_name;
        std::string m_obstacle_name;
        std::string m_planet_name;
        int m_level;
        int m_shoot_buffer_1 = 0;
        int m_shoot_buffer_2 = 0;
        int m_shoot_buffer_max;

    };

}

#endif //GP_PROJECT_GAMEMODEL_H

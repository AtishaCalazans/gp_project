//
// Created by atisha on 7/21/18.
//

#include "MenuModel.h"
#include "View.h"
#include "Button.h"
#include "Stopwatch.h"
#include <iostream>

namespace logic {
    
    MenuModel::MenuModel(shared_ptr<draw::View> view) :Model(view) {
        m_game_status = menu;
        m_select_buffer_max = singleton::Stopwatch::get_instance().get_fps() / 5;
        m_wait_buffer = double(singleton::Stopwatch::get_instance().get_fps()) / 2.0;
    }

    MenuModel::~MenuModel() {
        m_view->flush(true);
    }

    void MenuModel::tick() {
        if (m_wait_buffer > 0) {
            m_wait_buffer -= 1;
        }
        if (m_select_buffer > 0) {
            m_select_buffer -= 1;
        }

        Model::tick();
    }
    
    void MenuModel::move_up(const bool player_one) {
        if (m_select_buffer == 0) {
            m_buttons[m_current_button]->set_button_status(rest);
            if (m_current_button == 0) {
                m_current_button = exit_b;
            } else {
                m_current_button = static_cast<selected_button>(m_current_button - 1);
            }
            m_buttons[m_current_button]->set_button_status(hover);

            m_select_buffer = m_select_buffer_max;
        }
    }
    
    void MenuModel::move_down(const bool player_one) {
        if (m_select_buffer == 0) {
            m_buttons[m_current_button]->set_button_status(rest);
            if (m_current_button == 3) {
                m_current_button = singleplayer_b;
            } else {
                m_current_button = static_cast<selected_button>(m_current_button + 1);
            }
            m_buttons[m_current_button]->set_button_status(hover);

            m_select_buffer = m_select_buffer_max;
        }
    }
    
    void MenuModel::move_right(const bool player_one) {}
    
    void MenuModel::move_left(const bool player_one) {}
    
    void MenuModel::action(const bool player_one) {
        if (m_wait_buffer == 0) {
            switch (m_current_button) {

            case singleplayer_b:
                m_game_status = game_status::start_singleplayer;
                break;

            case multiplayer_b:
                m_game_status = game_status::start_multiplayer;
                break;

            case highscores_b:
                m_game_status = game_status::highscores;
                break;

            case exit_b:
                m_game_status = game_status::exit;
                break;

            }
        }
    }
    
    void MenuModel::initialize_entities() {
        auto text = std::make_shared<Text>("GRAYDIYUS", 50, 0, -2);
        m_text.push_back(text);
        m_entities.push_back(text);
        m_view->add_text(text);

        std::string button_text = "Singleplayer";
        auto button = std::make_shared<Button>(button_text, 25, 0, -0.5);
        button->set_button_status(hover);
        m_buttons.push_back(button);
        m_entities.push_back(button);
        m_view->add_button(button);
        
        button_text = "Multiplayer";
        button = std::make_shared<Button>(button_text, 25, 0, 0.5);
        m_buttons.push_back(button);
        m_entities.push_back(button);
        m_view->add_button(button);
        
        button_text = "Highscores";
        button = std::make_shared<Button>(button_text, 25, 0, 1.5);
        m_buttons.push_back(button);
        m_entities.push_back(button);
        m_view->add_button(button);
        
        button_text = "Exit";
        button = std::make_shared<Button>(button_text, 25, 0, 2.5);
        m_buttons.push_back(button);
        m_entities.push_back(button);
        m_view->add_button(button);
    }
    
    void MenuModel::delete_dead_entities() {}

}
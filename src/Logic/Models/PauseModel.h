//
// Created by atisha on 7/25/18.
//

#ifndef GP_PROJECT_PAUSEMODEL_H
#define GP_PROJECT_PAUSEMODEL_H

#include "Model.h"

namespace logic {

    class Text;
    class Button;

    /**
     * @brief a model that is used when the game is paused
     */
    class PauseModel: public Model {
    public:

        PauseModel(shared_ptr<draw::View>& view);

        ~PauseModel();

        /**
         * @brief make all calculations for one frame and notify the view
         */
        void tick() override;

        /**
         * @brief move up in the pause screen
         */
        void move_up(const bool player_one) override;

        /**
         * @brief move down in the pause screen
         */
        void move_down(const bool player_one) override;

        /**
         * @brief select the current button
         */
        void action(const bool player_one) override;

        /**
         * @brief initialize all entitites for this model
         */
        void initialize_entities() override;

        /**
         * @brief delete all dead entitites
         */
        void delete_dead_entities() override;

        /**
         * @brief delete all entities
         * @param hard_flush true if textures have to be removed as well
         */
        void flush(const bool hard_flush) override;

    private:

        enum selected_button {
        return_b,
        keybindings_b,
        exit_b
        };

        selected_button m_current_button = return_b;
        int m_select_buffer = 0;
        int m_select_buffer_max;

    };

}

#endif //GP_PROJECT_PAUSEMODEL_H

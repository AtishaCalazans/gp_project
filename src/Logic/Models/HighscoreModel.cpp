//
// Created by atisha on 8/22/18.
//
#include <Keybindings.h>
#include "HighscoreModel.h"
#include "View.h"
#include "Text.h"
#include "Highscores.h"
#include "Stopwatch.h"

namespace logic {

    HighscoreModel::HighscoreModel(shared_ptr<draw::View> &view) : Model(view) {
        m_game_status = highscores;
        m_wait_buffer = singleton::Stopwatch::get_instance().get_fps() * 1;
    }

    HighscoreModel::~HighscoreModel() {
        m_view->flush(true);
    }

    void HighscoreModel::tick() {
        if (m_wait_buffer > 0) m_wait_buffer -= 1;
        //let the user know that he can input his/her name
        if (m_wait_buffer == 0 and m_input) {
            auto text_2 = std::make_shared<Text>("Enter Name:", 20, 0, -1);
            m_text.push_back(text_2);
            m_view->add_text(text_2);
        }

        //update positions and status of entity
        for (auto& entity: m_text) {
            entity->update_observers();
        }

        m_view->draw();
    }

    void HighscoreModel::action(const bool player_one) {
        if (player_one) {
            if (m_input and !m_input_text->get_text().empty()) {
                //submit the name and move on to the highscore display
                singleton::Highscores::get_instance().save_score(m_input_text->get_text());
                singleton::Highscores::get_instance().write_to_file();
                m_input = false;
                m_wait_buffer = singleton::Stopwatch::get_instance().get_fps() * 2;
                Model::flush(true);
                initialize_highscore_screen();
            } else if (!m_input and m_wait_buffer == 0) {
                //move on to the menu
                m_wait_buffer = singleton::Stopwatch::get_instance().get_fps() * 2;
                Model::flush(true);
                m_game_status = menu;
            }
        }
    }

    void HighscoreModel::key_string_pressed(const string &key_str) {
        if (m_input and m_wait_buffer == 0) {
            //allow a maximum of 6 characters
            if (m_current_amount_of_characters < 6) {
                string new_input = m_input_text->get_text() + key_str;
                m_input_text->set_text(new_input);
                ++m_current_amount_of_characters;
            }
        }
    }

    void HighscoreModel::initialize_entities() {
        std::vector<std::tuple<std::string, int>> top_10 = singleton::Highscores::get_instance().m_top_10;

        if (singleton::Highscores::get_instance().m_current_score > std::get<1>(top_10[9])) {
            // if the current score is a new top 10 score
            m_input = true;
            initialize_input_screen();
        } else {
            initialize_highscore_screen();
        }
    }

    void HighscoreModel::initialize_input_screen(){
        //highscore text
        auto text_1 = std::make_shared<Text>("NEW HIGHSCORE!", 30, 0, -2);
        m_text.push_back(text_1);
        m_view->add_text(text_1);

        //starts as empty but the string will change as the users input characters
        auto input_text = std::make_shared<Text>("", 20, 0, 0);
        m_input_text = input_text;
        m_text.push_back(input_text);
        m_view->add_text(input_text);

        //confirm text
        auto continue_text = std::make_shared<Text>("Press " + controller::Keybindings::get_instance().get_str_action_key(controller::action_1)
                + " to confirm", 15, 0, 1);
        m_text.push_back(continue_text);
        m_view->add_text(continue_text);
    }

    void HighscoreModel::initialize_highscore_screen() {
        //fetch the scores and create the entities for them
        std::vector<std::tuple<std::string, int>> top_10 = singleton::Highscores::get_instance().m_top_10;
        for (int i = 0; i < top_10.size(); ++i) {
            string score_string = std::get<0>(top_10[i]) + ": " + std::to_string(std::get<1>(top_10[i]));
            auto text = std::make_shared<Text>(score_string, 18, 0, -2.3 + (i * 0.5));
            m_text.push_back(text);
            m_view->add_text(text);
        }
    }

}
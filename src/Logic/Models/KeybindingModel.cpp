//
// Created by atisha on 8/23/18.
//
#include "KeybindingModel.h"
#include "Button.h"
#include "View.h"
#include "Keybindings.h"
#include "Stopwatch.h"

namespace logic {

    KeybindingModel::KeybindingModel(shared_ptr<draw::View> &view) : Model(view) {
        m_game_status = keybindings;
        m_current_button = up_1;
        m_select_buffer_max = singleton::Stopwatch::get_instance().get_fps() / 5;
        m_select_buffer = m_select_buffer_max;
    }

    KeybindingModel::~KeybindingModel() {
        for (auto &entity: m_entities) {
            entity->lose_lives(1);
            entity->update_observers();
        }
    }

    void KeybindingModel::tick() {
        if (m_select_buffer > 0) {
            m_select_buffer -= 1;
        }
        Model::tick();
    }

    void KeybindingModel::initialize_entities() {
        //add left buttons
        add_button("P1 Up", 15, -2, -2.5);
        m_buttons[0]->set_button_status(hover);
        add_button("P1 Down", 15, -2, -2.1);
        add_button("P1 Right", 15, -2, -1.7);
        add_button("P1 Left", 15, -2, -1.3);
        add_button("P1 Action", 15, -2, -0.9);
        add_button("P2 Up", 15, -2, -0.5);
        add_button("P2 Down", 15, -2, -0.1);
        add_button("P2 Right", 15, -2, 0.3);
        add_button("P2 Left", 15, -2, 0.7);
        add_button("P2 Action", 15, -2, 1.1);
        add_button("Pause", 15, -2, 1.5);
        add_button("Debug", 15, -2, 1.9);

        //add return buttons
        add_button("Default and return", 17, 0, 2.2);
        add_button("Save and return", 17, 0, 2.7);

        //add right buttons
        add_button(controller::Keybindings::get_instance().get_str_action_key(controller::move_up_1), 15, 2, -2.5);
        add_button(controller::Keybindings::get_instance().get_str_action_key(controller::move_down_1), 15, 2, -2.1);
        add_button(controller::Keybindings::get_instance().get_str_action_key(controller::move_right_1), 15, 2, -1.7);
        add_button(controller::Keybindings::get_instance().get_str_action_key(controller::move_left_1), 15, 2, -1.3);
        add_button(controller::Keybindings::get_instance().get_str_action_key(controller::action_1), 15, 2, -0.9);
        add_button(controller::Keybindings::get_instance().get_str_action_key(controller::move_up_2), 15, 2, -0.5);
        add_button(controller::Keybindings::get_instance().get_str_action_key(controller::move_down_2), 15, 2, -0.1);
        add_button(controller::Keybindings::get_instance().get_str_action_key(controller::move_right_2), 15, 2, 0.3);
        add_button(controller::Keybindings::get_instance().get_str_action_key(controller::move_left_2), 15, 2, 0.7);
        add_button(controller::Keybindings::get_instance().get_str_action_key(controller::action_2), 15, 2, 1.1);
        add_button(controller::Keybindings::get_instance().get_str_action_key(controller::pause), 15, 2, 1.5);
        add_button(controller::Keybindings::get_instance().get_str_action_key(controller::debug), 15, 2, 1.9);
    }

    void KeybindingModel::add_button(std::string text, int size, float x, float y) {
        auto button = std::make_shared<Button>(text, size, x, y);
        m_buttons.push_back(button);
        m_entities.push_back(button);
        m_view->add_button(button);
    }

    void KeybindingModel::move_up(const bool player_one) {
        if (m_select_buffer == 0 and !m_input) {
            m_buttons[m_current_button]->set_button_status(rest);
            if (m_current_button < 12) m_buttons[m_current_button + 14]->set_button_status(rest);
            if (m_current_button == 0) {
                m_current_button = return_pause;
            } else {
                m_current_button = static_cast<selected_button>(m_current_button - 1);
            }
            m_buttons[m_current_button]->set_button_status(hover);

            m_select_buffer = m_select_buffer_max;
        }
    }

    void KeybindingModel::move_down(const bool player_one) {
        if (m_select_buffer == 0 and !m_input) {
            m_buttons[m_current_button]->set_button_status(rest);
            if (m_current_button < 12) m_buttons[m_current_button + 14]->set_button_status(rest);
            if (m_current_button == 13) {
                m_current_button = up_1;
            } else {
                m_current_button = static_cast<selected_button>(m_current_button + 1);
            }
            m_buttons[m_current_button]->set_button_status(hover);

            m_select_buffer = m_select_buffer_max;
        }
    }

    void KeybindingModel::action(const bool player_one) {
        if (m_select_buffer == 0){
            if (m_current_button < 12) {
                m_buttons[m_current_button + 14]->set_button_status(hover);
                m_input = !m_input;
                m_select_buffer = m_select_buffer_max;
            } else if (m_current_button == 12) {
                controller::Keybindings::get_instance().set_default();
                controller::Keybindings::get_instance().write_to_file();
                m_game_status = game_status::playing;
            } else {
                controller::Keybindings::get_instance().write_to_file();
                m_game_status = game_status::playing;
            }
        }
    }

    void KeybindingModel::key_pressed(const sf::Keyboard::Key key) {
        if (m_input) {
            auto action = static_cast<controller::action>(m_current_button);
            controller::Keybindings::get_instance().change_keybinding(action, key);
            string new_key_str = controller::Keybindings::get_instance().get_str_action_key(action);
            m_buttons[m_current_button + 14]->set_text(new_key_str);
            m_buttons[m_current_button + 14]->set_button_status(rest);
            m_input = false;
        }
    }

}

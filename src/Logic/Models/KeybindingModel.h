//
// Created by atisha on 8/23/18.
//

#ifndef GP_PROJECT_KEYBINDINGMODEL_H
#define GP_PROJECT_KEYBINDINGMODEL_H

#include "Model.h"

namespace logic {

    /**
     * @brief class used for the editing of keybindings
     */
    class KeybindingModel: public Model {
    public:

        KeybindingModel(shared_ptr<draw::View>& view);

        ~KeybindingModel();

        /**
         * @brief make all calculations for one frame and notify the view
         */
        void tick() override;

        /**
         * @brief move up in the menu
         */
        void move_up(const bool player_one) override;

        /**
         * @brief move down in the menu
         */
        void move_down(const bool player_one) override;

        /**
         * @brief select the current button selected
         */
        void action(const bool player_one) override;

        /**
         * @brief initialize all entities
         */
        void initialize_entities() override;

        /**
         * @brief let the model know what key is being pressed
         */
        virtual void key_pressed(const sf::Keyboard::Key key) override;

    private:

        enum selected_button {
            up_1,
            down_1,
            right_1,
            left_1,
            action_1,
            up_2,
            down_2,
            right_2,
            left_2,
            action_2,
            pause,
            debug,
            set_default,
            return_pause
        };

        selected_button m_current_button;
        int m_select_buffer = 0;
        int m_select_buffer_max;
        bool m_input = false;

        /**
         * @brief adds a button to the model
         * @param text the text of the button
         * @param size character size of the text
         * @param x x_position
         * @param y y_position
         */
        void add_button(std::string text, int size, float x, float y);

    };

}

#endif //GP_PROJECT_KEYBINDINGMODEL_H

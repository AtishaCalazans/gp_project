//
// Created by atisha on 7/25/18.
//

#include "PauseModel.h"
#include "View.h"
#include "Button.h"
#include "Stopwatch.h"
#include "BaseException.h"
#include <iostream>

namespace logic {
    
    PauseModel::PauseModel(shared_ptr<draw::View> &view) :Model(view) {
        m_game_status = pause;
        m_select_buffer_max = singleton::Stopwatch::get_instance().get_fps() / 10;
    }

    PauseModel::~PauseModel() {
        for (auto &button: m_buttons) {
            button->lose_lives(1);
            button->update_observers();
        }
    }

    void PauseModel::tick() {
        if (m_select_buffer > 0) {
            m_select_buffer -= 1;
        }

        //update positions and status of entity
        for (auto& entity: m_buttons) {
            entity->update_observers();
        }
        m_view->draw();
    }
    
    void PauseModel::move_up(const bool player_one) {
        if (m_select_buffer == 0) {
            m_buttons[m_current_button]->set_button_status(rest);
            if (m_current_button == 0) {
                m_current_button = exit_b;
            } else {
                m_current_button = static_cast<selected_button>(m_current_button - 1);
            }
            m_buttons[m_current_button]->set_button_status(hover);

            m_select_buffer = m_select_buffer_max;
        }
    }
    
    void PauseModel::move_down(const bool player_one) {
        if (m_select_buffer == 0) {
            m_buttons[m_current_button]->set_button_status(rest);
            if (m_current_button == 2) {
                m_current_button = return_b;
            } else {
                m_current_button = static_cast<selected_button>(m_current_button + 1);
            }
            m_buttons[m_current_button]->set_button_status(hover);

            m_select_buffer = m_select_buffer_max;
        }
    }
    
    void PauseModel::action(const bool player_one) {
        switch (m_current_button) {
                
            case return_b:
                m_game_status = game_status::playing;
                break;

            case keybindings_b:
                m_game_status = game_status::keybindings;
                break;
                
            case exit_b:
                m_game_status = game_status::exit;
                break;

            default:
                throw exception::EnumDomainError("Invalid button in PauseModel");
        }
    }
    
    void PauseModel::initialize_entities() {
        std::string button_text = "Return";
        auto button = std::make_shared<Button>(button_text, 25, 0, -1);
        button->set_button_status(hover);
        m_buttons.push_back(button);
        m_view->add_button(button);
        
        button_text = "Keybindings";
        button = std::make_shared<Button>(button_text, 25, 0, 0);
        m_buttons.push_back(button);
        m_view->add_button(button);
        
        button_text = "Exit";
        button = std::make_shared<Button>(button_text, 25, 0, 1);
        m_buttons.push_back(button);
        m_view->add_button(button);
    }
    
    void PauseModel::delete_dead_entities() {}

    //this is empty because the pause model is not allowed do delete entities, the game has to be resumed afterwards
    void PauseModel::flush(const bool hard_flush) {}
    
}
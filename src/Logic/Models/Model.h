//
// Created by atisha on 03.12.17.
//

#ifndef GP_PROJECT_MODEL_H
#define GP_PROJECT_MODEL_H

#include "../json.hpp"
#include "Text.h"
#include <vector>
#include <memory>
#include <list>

using std::vector;
using std::shared_ptr;
using std::list;
using nlohmann::json;
namespace draw {class View;}
namespace controller {class Controller;}

/**
 * @brief namespace that contains all the logic and data
 */
namespace logic {

    class Entity;
    class Text;
    class Button;

    /**
     * @brief enum used to describe the state the game is in
     */
    enum game_status {
        menu,
        start_singleplayer,
        start_multiplayer,
        highscores,
        playing,
        game_over,
        next_level,
        pause,
        keybindings,
        exit
    };

    /**
     * @brief class that contains all entities and communicates with View to display these entities
     */
    class Model {
    public:

        Model(shared_ptr<draw::View> view);

        ~Model() = default;

        /**
         * @brief make all calculations for one frame and notify the view
         */
        virtual void tick();

        /**
         * @brief up key pressed
         */
        virtual void move_up(const bool player_one) {}

        /**
         * @brief down key pressed
         */
        virtual void move_down(const bool player_one) {}

        /**
         * @brief right key pressed
         */
        virtual void move_right(const bool player_one) {}

        /**
         * @brief left key pressed
         */
        virtual void move_left(const bool player_one) {}

        /**
         * @brief action key pressed
         */
        virtual void action(const bool player_one) {}

        /**
         * @brief give the model a string representation of the key being pressed (only characters)
         */
         virtual void key_string_pressed(const string& key_str);

         /**
          * @brief let the model know what key is being pressed
          */
         virtual void key_pressed(const sf::Keyboard::Key);

        /**
         * @brief creates entities and places them in the correct position
         */
        virtual void initialize_entities() = 0;

        /**
         * @brief switch debug mode on/off
         */
        void switch_debug();

        /**
         * @param multiplayer true if the game should be multiplayer
         */
        void set_multiplayer(const bool multiplayer);

        /**
         * @brief deletes all dead entities
         */
        virtual void delete_dead_entities();

        /**
         * @brief delete all entities
         * @param hard_flush true if textures have to be removed as well
         */
        virtual void flush(const bool hard_flush);

        /**
         * @return the status of the game as the enum type 'game_status'
         */
        game_status get_game_status();

    protected:

        shared_ptr<draw::View> m_view;
        list<std::shared_ptr<Entity>> m_entities;
        list<std::shared_ptr<Text>> m_text;
        vector<std::shared_ptr<Button>> m_buttons;
        game_status m_game_status = menu;
        bool m_multiplayer = false;

    };

}

#endif //GP_PROJECT_MODEL_H

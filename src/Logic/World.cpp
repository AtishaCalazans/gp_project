//
// Created by atisha on 11.01.18.
//

#include "World.h"
#include "GameSpecs.h"

namespace logic {

    World::World(const float& x, const float& y, const string& texture_name)
            : Loop(x, y, singleton::GameSpecs::get_instance().speeds.world) {
        m_texture_name = texture_name;
    }

    string World::get_texture_name() const {
        return m_texture_name;
    }

}
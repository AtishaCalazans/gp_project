//
// Created by atisha on 11.01.18.
//

#ifndef GP_PROJECT_WORLD_H
#define GP_PROJECT_WORLD_H

#include "Loop.h"

namespace logic {

    /**
     * @brief class used for the world border at the top and bottom of the screen
     */
    class World : public Loop {
    public:

        World(const float& x, const float& y, const string& texture_name);

        string get_texture_name() const override;

    private:

        string m_texture_name;

    };

}

#endif //GP_PROJECT_WORLD_H

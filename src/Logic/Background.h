//
// Created by atisha on 29.12.17.
//

#ifndef GP_PROJECT_BACKGROUND_H
#define GP_PROJECT_BACKGROUND_H

#include "Loop.h"

namespace logic {

    /**
     * @brief class used to represent the space background
     */
    class Background : public Loop {
    public:

        Background(const float& x, const float& y);

        string get_texture_name() const override;

    };

}

#endif //GP_PROJECT_BACKGROUND_H

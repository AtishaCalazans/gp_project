//
// Created by atisha on 7/19/18.
//

#ifndef GP_PROJECT_TEXT_H
#define GP_PROJECT_TEXT_H

#include "Entity.h"
#include <string>

namespace draw {class TextView;}

namespace logic {

    /**
     * @brief class used to represent text
     */
    class Text: public Entity {
    public:

        Text() = default;

        Text(const std::string text, const unsigned int char_size, const float x, const float y);

        std::string get_text() const;

        unsigned int get_char_size() const;

        sf::Uint8 get_visibility() const;

        void set_text(const std::string& new_text);

        void move() override;

    protected:

        std::string m_text;
        unsigned int m_char_size = 1;
        double m_visibility = 255;

    };

}

#endif //GP_PROJECT_TEXT_H

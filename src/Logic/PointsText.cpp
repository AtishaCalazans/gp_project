//
// Created by atisha on 7/19/18.
//

#include "PointsText.h"
#include "Stopwatch.h"

namespace logic {

    PointsText::PointsText(const shared_ptr<logic::Entity> entity, const int points_gained)
            : Text(std::to_string(points_gained), 17, entity->get_x() - entity->get_hitbox_radius(), entity->get_y() + entity->get_hitbox_radius()){
        m_remaining_lifespan = singleton::Stopwatch::get_instance().get_fps() / 2;
        m_visibility_change_per_frame = 255.0 / (singleton::Stopwatch::get_instance().get_fps() / 2);

    }

    void PointsText::update_observers() {
        if (m_remaining_lifespan == 0) {
            m_status = dead;
        } else {
            m_remaining_lifespan -= 1;
            m_visibility -= m_visibility_change_per_frame;
            m_rotation -= 1;
        }
        Entity::update_observers();

    }

}
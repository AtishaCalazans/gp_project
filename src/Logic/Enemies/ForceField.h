//
// Created by atisha on 8/21/18.
//

#ifndef GP_PROJECT_FORCEFIELD_H
#define GP_PROJECT_FORCEFIELD_H

#include "Enemy.h"

namespace logic {

    /**
     * @brief force field that surrounds Force Enemy
     */
    class ForceField: public Enemy {
    public:

        ForceField(const float& x, const float& y);

        string get_texture_name() const override;

        void move() override;

        void lose_lives(const int& lives) override;

        void update_observers() override;

    };

}

#endif //GP_PROJECT_FORCEFIELD_H

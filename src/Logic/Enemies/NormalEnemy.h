//
// Created by atisha on 12.01.18.
//

#ifndef GP_PROJECT_NORMAL_ENEMY_H
#define GP_PROJECT_NORMAL_ENEMY_H

#include "Enemy.h"

namespace logic {

    /**
     * @brief an enemy that goes up and down and shoots forward
     */
    class NormalEnemy : public Enemy {
    public:

        NormalEnemy(const float& x, const float& y);

        string get_texture_name() const override;

        void move() override ;

        void move_y(const bool& positive) override;

        void update_observers() override;

    private:

        bool m_moving_up;

    };

}

#endif //GP_PROJECT_NORMAL_ENEMY_H

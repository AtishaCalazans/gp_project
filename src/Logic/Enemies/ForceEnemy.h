//
// Created by atisha on 8/21/18.
//

#ifndef GP_PROJECT_FORCEENEMY_H
#define GP_PROJECT_FORCEENEMY_H

#include "Enemy.h"

namespace logic {

    class ForceField;

    /**
     * @brief enemy that has a forcefield surrounding it
     */
    class ForceEnemy : public Enemy {
    public:

        ForceEnemy(const float& x, const float& y);

        string get_texture_name() const override;

        void move() override;

        void update_observers() override;

    private:



    };

}

#endif //GP_PROJECT_FORCEENEMY_H

//
// Created by atisha on 8/8/18.
//

#ifndef GP_PROJECT_LASERENEMY_H
#define GP_PROJECT_LASERENEMY_H

#include "Enemy.h"

namespace logic {

    /**
     * @brief an enemy that shoots a laser
     */
    class LaserEnemy : public Enemy{
    public:

        LaserEnemy(const float& x, const float& y);

        string get_texture_name() const override;

        void move() override;

        void move_y(const bool& positive) override;

        void update_observers() override;

    private:

        float m_y_destination;

    };

}

#endif //GP_PROJECT_LASERENEMY_H

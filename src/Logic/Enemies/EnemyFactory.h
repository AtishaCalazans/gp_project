//
// Created by atisha on 12.01.18.
//

#ifndef GP_PROJECT_ENEMYFACTORY_H
#define GP_PROJECT_ENEMYFACTORY_H

#include <memory>
#include "Entity.h"

namespace logic {

    class Enemy;

    /**
     * @brief enum used to describe all types of enemies
     */
    enum enemy_type {
        normal,
        quick,
        homing,
        laser,
        force
    };

    /**
     * @brief factory for creating enemies (some enemies were not implemented)
     */
    class EnemyFactory {
    public:

        /**
         * @brief creates an enemy of a certain type, the enemies that were not implemented default to NormalEnemy
         */
        static std::shared_ptr<Enemy> get_enemy(const float& x, const float& y, enemy_type type);

    };

}

#endif //GP_PROJECT_ENEMYFACTORY_H

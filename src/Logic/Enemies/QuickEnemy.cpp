//
// Created by atisha on 13.01.18.
//

#include "QuickEnemy.h"
#include "GameSpecs.h"

namespace logic {

    QuickEnemy::QuickEnemy(const float &x, const float &y) : Enemy(x, y, singleton::GameSpecs::get_instance().hitboxes.quick_enemy) {
        m_speed = singleton::GameSpecs::get_instance().speeds.quick_enemy;
        //give 100 lives to make it practically indestructible
        m_lives = singleton::GameSpecs::get_instance().lives.quick_enemy;
        update_travel_distance_per_frame();
    }

    string QuickEnemy::get_texture_name() const {
        return "quick_enemy.png";
    }

    void QuickEnemy::move() {
        move_x(false);
    }

}
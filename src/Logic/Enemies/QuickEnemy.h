//
// Created by atisha on 13.01.18.
//

#ifndef GP_PROJECT_QUICKENEMY_H
#define GP_PROJECT_QUICKENEMY_H

#include "Enemy.h"

namespace logic {

    /**
     * @brief an enemy that doesn't shoot but charges forward very quickly
     */
    class QuickEnemy : public Enemy {
    public:

        QuickEnemy(const float& x, const float& y);

        string get_texture_name() const override;

        void move() override;

    };

}

#endif //GP_PROJECT_QUICKENEMY_H

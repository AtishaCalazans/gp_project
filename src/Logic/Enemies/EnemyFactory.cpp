//
// Created by atisha on 12.01.18.
//

#include <BaseException.h>
#include "EnemyFactory.h"
#include "NormalEnemy.h"
#include "QuickEnemy.h"
#include "HomingEnemy.h"
#include "LaserEnemy.h"
#include "ForceEnemy.h"
#include "BaseException.h"

namespace logic {

    std::shared_ptr<Enemy> EnemyFactory::get_enemy(const float& x, const float& y, enemy_type type) {

        switch (type) {
            case normal:
                return std::make_shared<NormalEnemy>(x, y);

            case quick:
                return std::make_shared<QuickEnemy>(x, y);

            case homing:
                return std::make_shared<HomingEnemy>(x, y);

            case laser:
                return std::make_shared<LaserEnemy>(x, y);

            case force:
                return std::make_shared<ForceEnemy>(x, y);

            default:
                throw exception::EnumDomainError("Invalid enemy type");
        }

    }

}

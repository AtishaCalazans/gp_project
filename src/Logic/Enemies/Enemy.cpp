//
// Created by atisha on 12.01.18.
//

#include "Enemy.h"

namespace logic {

    Enemy::Enemy(const float &x, const float &y, const float &radius) : Entity(x, y, radius) {}

    bullet_type Enemy::is_shooting() const {
        return m_bullet_type;
    }

}
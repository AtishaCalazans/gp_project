//
// Created by atisha on 13.01.18.
//

#ifndef GP_PROJECT_HOMINGENEMY_H
#define GP_PROJECT_HOMINGENEMY_H

#include "Enemy.h"

namespace logic {

    /**
     * @brief a large enemy that doesn't move but shoots homing missiles
     */
    class HomingEnemy : public Enemy {
    public:

        HomingEnemy(const float& x, const float& y);

        string get_texture_name() const override;

        void move() override;

        void update_observers() override;

    };

}

#endif //GP_PROJECT_HOMINGENEMY_H

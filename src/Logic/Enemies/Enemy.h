//
// Created by atisha on 12.01.18.
//

#ifndef GP_PROJECT_ENEMY_H
#define GP_PROJECT_ENEMY_H

#include "Entity.h"

namespace logic {

    /**
     * @brief enum used to describe the bullet type that has to be fired, 'none' just means that no bullet has
     * to be fired at that moment
     */
    enum bullet_type {
        none,
        normal_b,
        homing_b,
        laser_b,
        force_b
    };

    /**
     * @brief class used for enemies
     */
    class Enemy : public Entity {
    public:

        Enemy(const float& x, const float& y, const float& radius);

        /**
         * @return returns the bullet type that has to be shot, if no bullet has to be shot, 'none' is returned
         */
        bullet_type is_shooting() const;

    protected:

        bullet_type m_bullet_type = none;
        double m_fire_rate = 0;
        int m_shooting_counter;

    };

}

#endif //GP_PROJECT_ENEMY_H

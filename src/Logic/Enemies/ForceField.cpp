//
// Created by atisha on 8/21/18.
//
#include "ForceField.h"
#include "GameSpecs.h"
#include "Stopwatch.h"

namespace logic {

    ForceField::ForceField(const float &x, const float &y)
    : Enemy(x, y, singleton::GameSpecs::get_instance().hitboxes.force_field) {
        m_speed = singleton::GameSpecs::get_instance().speeds.force_field;
        m_lives = singleton::GameSpecs::get_instance().lives.force_field;
        update_travel_distance_per_frame();
    }

    string ForceField::get_texture_name() const {
        return singleton::GameSpecs::get_instance().textures.force_field;
    }

    void ForceField::move() {
        if (m_x > 3.5) {
            move_x(false);
        }
        //move until y coordinate is 0
        if (m_y > 0) {
            m_y -= m_move_y_per_frame;
            if (m_y < 0) m_y = 0;
        } else if (m_y < 0) {
            m_y += m_move_y_per_frame;
            if (m_y > 0) m_y = 0;
        }
    }

    void ForceField::lose_lives(const int &lives){
        m_lives -= lives;
        //become more transparent as it gets weaker
        m_visibility = float(m_lives) / float(singleton::GameSpecs::get_instance().lives.force_field);
        if (m_lives == 0) m_status = dead;
    }

    void ForceField::update_observers() {
        m_rotation += 10.0 / singleton::Stopwatch::get_instance().get_fps();
        Entity::update_observers();
    }

}

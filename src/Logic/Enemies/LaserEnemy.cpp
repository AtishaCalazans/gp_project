//
// Created by atisha on 8/8/18.
//
#include "LaserEnemy.h"
#include "GameSpecs.h"
#include "Stopwatch.h"

namespace logic {

    LaserEnemy::LaserEnemy(const float &x, const float &y)
    :Enemy(x, y, singleton::GameSpecs::get_instance().hitboxes.laser_enemy) {
        m_fire_rate = singleton::GameSpecs::get_instance().bullet_rates.laser_enemy;
        m_speed = singleton::GameSpecs::get_instance().speeds.laser_enemy;
        m_fire_rate = singleton::GameSpecs::get_instance().bullet_rates.laser_enemy;
        m_lives = singleton::GameSpecs::get_instance().lives.laser_enemy;
        m_shooting_counter = int(singleton::Stopwatch::get_instance().get_fps() / m_fire_rate);
        update_travel_distance_per_frame();
    }

    string LaserEnemy::get_texture_name() const {
        return singleton::GameSpecs::get_instance().textures.laser_enemy;
    }

    void LaserEnemy::move() {
        if (m_x > 3.5) {
            move_x(false);
        } else if (m_shooting_counter < singleton::Stopwatch::get_instance().get_fps() * 2) {
            if (m_y_destination == -3) {
                srand(time(NULL));
                m_y_destination = -1.75 + ((rand() % 9) * 0.41);
            }
            move_y(true);
        }
    }

    void LaserEnemy::move_y(const bool &positive) {
        bool move_up = m_y <= m_y_destination;
        if (move_up) m_y += m_move_y_per_frame;
        else m_y -= m_move_y_per_frame;

        //if the current position relative to the destination is different after moving
        if (move_up != m_y < m_y_destination) {
            m_y = m_y_destination;
            // set to -3 to let move() function know that it stopped moving
            m_y_destination = -3;
        }
    }

    void LaserEnemy::update_observers() {
        if (m_shooting_counter == 0) {
            m_shooting_counter = int(singleton::Stopwatch::get_instance().get_fps() / m_fire_rate);
            m_bullet_type = none;
        }
        else {
            --m_shooting_counter;
            if (m_shooting_counter < singleton::Stopwatch::get_instance().get_fps() * 2 and m_shooting_counter % 2 == 0) m_bullet_type = laser_b;
            else m_bullet_type = none;
        }
        Entity::update_observers();
    }

}

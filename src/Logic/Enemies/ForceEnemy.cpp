//
// Created by atisha on 8/21/18.
//

#include "ForceEnemy.h"
#include "GameSpecs.h"
#include "Stopwatch.h"

namespace logic {

    ForceEnemy::ForceEnemy(const float &x, const float &y)
    : Enemy(x, y, singleton::GameSpecs::get_instance().hitboxes.force_enemy){
        m_fire_rate = singleton::GameSpecs::get_instance().bullet_rates.force_enemy;
        m_speed = singleton::GameSpecs::get_instance().speeds.force_enemy;
        m_fire_rate = singleton::GameSpecs::get_instance().bullet_rates.force_enemy;
        m_lives = singleton::GameSpecs::get_instance().lives.force_enemy;
        m_shooting_counter = int(singleton::Stopwatch::get_instance().get_fps() / m_fire_rate);
        m_bullet_type = force_b;
        update_travel_distance_per_frame();
    }

    string ForceEnemy::get_texture_name() const {
        return singleton::GameSpecs::get_instance().textures.force_enemy;
    }

    void ForceEnemy::move() {
        if (m_x > 3.5) {
            move_x(false);
        }
        //move until it has y position 0
        if (m_y > 0) {
            m_y -= m_move_y_per_frame;
            if (m_y < 0) m_y = 0;
        } else if (m_y < 0) {
            m_y += m_move_y_per_frame;
            if (m_y > 0) m_y = 0;
        }
    }

    void ForceEnemy::update_observers() {
        m_bullet_type = none;
        if (m_shooting_counter == 0) {
            //shoot faster when the forcefield is destroyed
            if (m_lives < singleton::GameSpecs::get_instance().lives.force_enemy) {
                m_shooting_counter = int(singleton::Stopwatch::get_instance().get_fps() / (m_fire_rate * 2));
            } else {
                m_shooting_counter = int(singleton::Stopwatch::get_instance().get_fps() / m_fire_rate);
            }
            m_bullet_type = none;
        } else {
            --m_shooting_counter;
            if (m_shooting_counter == 0) m_bullet_type = force_b;
        }
        Entity::update_observers();
    }

}

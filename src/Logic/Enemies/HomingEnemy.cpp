//
// Created by atisha on 13.01.18.
//

#include "HomingEnemy.h"
#include "Stopwatch.h"
#include "GameSpecs.h"

namespace logic {

    HomingEnemy::HomingEnemy(const float& x, const float& y)
    : Enemy(x, y, singleton::GameSpecs::get_instance().hitboxes.homing_enemy) {
        m_fire_rate = singleton::GameSpecs::get_instance().bullet_rates.homing_enemy;
        m_shooting_counter = int(singleton::Stopwatch::get_instance().get_fps() / m_fire_rate);
        m_lives = singleton::GameSpecs::get_instance().lives.homing_enemy;
        m_speed = singleton::GameSpecs::get_instance().speeds.homing_enemy;
        update_travel_distance_per_frame();
    }

    string HomingEnemy::get_texture_name() const {
        return singleton::GameSpecs::get_instance().textures.homing_enemy;
    }

    void HomingEnemy::move() {
        if (m_x > 3.5) {
            move_x(false);
        }
    }

    void HomingEnemy::update_observers() {
        if (m_shooting_counter == 0) {
            m_shooting_counter = int(singleton::Stopwatch::get_instance().get_fps() / m_fire_rate);
            m_bullet_type = none;
        }
        else {
            --m_shooting_counter;
            if (m_shooting_counter == 0) m_bullet_type = homing_b;
        }
        Entity::update_observers();
    }

}
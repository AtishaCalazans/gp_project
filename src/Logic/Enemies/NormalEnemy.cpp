//
// Created by atisha on 12.01.18.
//

#include <Stopwatch.h>
#include "NormalEnemy.h"
#include "GameSpecs.h"

namespace logic {

    NormalEnemy::NormalEnemy(const float& x, const float& y)
    : Enemy(x, y, singleton::GameSpecs::get_instance().hitboxes.normal_enemy) {
        m_fire_rate = singleton::GameSpecs::get_instance().bullet_rates.normal_enemy;
        m_shooting_counter = int(singleton::Stopwatch::get_instance().get_fps() / m_fire_rate);
        m_moving_up = true;
        m_lives = singleton::GameSpecs::get_instance().lives.normal_enemy;
        m_speed = singleton::GameSpecs::get_instance().speeds.normal_enemy;
        update_travel_distance_per_frame();
    }

    string NormalEnemy::get_texture_name() const {
        return "normal_enemy.png";
    }

    void NormalEnemy::move() {
        if (m_x > 3.5) {
            move_x(false);
        } else {
            move_y(m_moving_up);
        }
    }

    void NormalEnemy::move_y(const bool& positive) {
        if (positive) {
            m_y += m_move_y_per_frame;
            if (m_y > 2.5) {
                m_y = 2.5;
                //turn the ship around
                m_moving_up = false;
            }
        } else {
            m_y -= m_move_y_per_frame;
            //if entity is off screen (up)
            if (m_y < -2.5) {
                m_y = -2.5;
                //turn the ship around
                m_moving_up = true;
            }
        }
    }

    void NormalEnemy::update_observers() {
        if (m_shooting_counter == 0) {
            m_shooting_counter = int(singleton::Stopwatch::get_instance().get_fps() / m_fire_rate);
            m_bullet_type = none;
        }
        else {
            --m_shooting_counter;
            if (m_shooting_counter == 0) m_bullet_type = normal_b;
        }
        Entity::update_observers();
    }

}
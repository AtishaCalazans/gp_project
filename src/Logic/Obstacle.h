//
// Created by atisha on 12.01.18.
//

#ifndef GP_PROJECT_OBSTACLE_H
#define GP_PROJECT_OBSTACLE_H

#include "Entity.h"

namespace logic {

    /**
     * @brief class used for obstacles that spawn randomly throughout the level
     */
    class Obstacle : public Entity {
    public:

        Obstacle(const float& x, const float& y, const string& texture_name);

        void move();

        string get_texture_name() const;

    private:

        string m_texture_name;

    };

}

#endif //GP_PROJECT_OBSTACLE_H

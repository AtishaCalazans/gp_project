//
// Created by atisha on 05.12.17.
//

#ifndef GP_PROJECT_GAME_H
#define GP_PROJECT_GAME_H

#include <memory>
#include <SFML/Graphics.hpp>

namespace draw {class View;}
namespace logic {class GameModel;}
namespace controller {class Controller;}

/**
 * @brief class that contains Controller, Model, View and thus the entire game
 */
class Game {
public:

    Game();

    /**
     * @brief run the game
     */
    void run();

    /**
     * @brief end the game and close the window
     */
    void close_window();

    /**
     * @brief this getter is primarily used by view
     * @return shared pointer to the view
     */
    std::shared_ptr<draw::View> get_view() const;

private:

    std::shared_ptr<controller::Controller> m_controller;
    std::shared_ptr<logic::GameModel> m_model;
    std::shared_ptr<draw::View> m_view;
    std::shared_ptr<sf::RenderWindow> m_window;
    std::string m_error_msg;

};

#endif //GP_PROJECT_GAME_H

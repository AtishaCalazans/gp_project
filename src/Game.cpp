//
// Created by atisha on 05.12.17.
//

#include "Game.h"
#include "Controller.h"
#include "GameModel.h"
#include "Stopwatch.h"
#include "Transformation.h"
#include "View.h"
#include "json.hpp"
#include "GameSpecs.h"
#include "Highscores.h"
#include <iostream>
#include <fstream>

using nlohmann::json;

Game::Game() {
    int fps;
    int res_x;
    int res_y;
    std::string m_error_message = "";
    try {
        //initialize the GameSpecs class
        singleton::GameSpecs::get_instance().initialize("../Resources/game_config.json");
        fps = singleton::GameSpecs::get_instance().fps;
        res_x = singleton::GameSpecs::get_instance().res_x;
        res_y = singleton::GameSpecs::get_instance().res_y;

        //initialize the Highscores class
        singleton::Highscores::get_instance().initialize();
    } catch (exception::FileException& f) {
        m_error_message = f.what();
        fps = 60;
        res_x = 800;
        res_y = 600;
    }

    //initialize the Stopwatch class
    singleton::Stopwatch::get_instance().set_fps(fps);

    //create the sfml window that will be used by View
    m_window = std::shared_ptr<sf::RenderWindow>(new sf::RenderWindow(sf::VideoMode(res_x, res_y), "Graydiyus"));

    //set the dimensions of the screen
    singleton::Transformation::get_instance().change_dimensions(m_window->getSize().x, m_window->getSize().y);

    //create the main classes for the game
    m_view = std::make_shared<draw::View>(m_window);
    m_model = std::make_shared<logic::GameModel>(m_view);
    m_controller = std::make_shared<controller::Controller>(this, m_model);

    //disable repeated KeyPressed events
    m_window->setKeyRepeatEnabled(false);

    if (!m_error_message.empty()) {
        m_view->display_error(m_error_message);
    }
}

void Game::run() {
    sf::Event event;

     while (m_window->isOpen()) {
        singleton::Stopwatch::get_instance().start();

        try {
            if (m_error_msg.empty()) {
                //if no exception was thrown
                while (m_window->pollEvent(event)) {
                m_controller->evaluate_event(event);
                }

                m_controller->notify_model();

            } else {
                //if an exception was thrown, display the error
                m_view->draw();
                while (m_window->pollEvent(event)) {
                    if (event.type == sf::Event::Closed) close_window();
                }
            }
        } catch (exception::FileException& f){
            m_error_msg = f.what();
            m_view->display_error(m_error_msg);
        }

        singleton::Stopwatch::get_instance().fill_remaining_time();
    }
}

void Game::close_window() {
    m_window->close();
    exit(0);
}

std::shared_ptr<draw::View> Game::get_view() const {
    return m_view;
}

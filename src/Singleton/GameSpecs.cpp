//
// Created by atisha on 7/9/18.
//

#include <fstream>
#include "GameSpecs.h"
#include "../json.hpp"
#include "BaseException.h"
#include <map>

using nlohmann::json;

namespace singleton {

    void GameSpecs::initialize(string json_file) {
        json config;
        try {
            std::ifstream file(json_file);
            file >> config;
        } catch (std::exception& e) {
            throw exception::FileReadingError("Unable to read file " + json_file);
        }

        std::string message_start = "In config file " + json_file + ": ";

        //read all file names
        try {
            json j_files = config["Files"];
            text_font = j_files["Font"];
            highscores = j_files["Highscores"];
            key_bindings = j_files["Keybindings"];
            texures_location = j_files["Textures"];
        } catch (std::domain_error& d) {
            throw exception::DomainError(message_start + "\"Files\" should only contain strings");
        }

        try {
            for (std::string level: config["Levels"]) {
                level_files.push_back(level);
            }
        } catch (std::domain_error& d){
            throw exception::DomainError(message_start + "\"Levels\" list should only contain strings");
        }

        //read resolution and frames per second
        try {
            json j_gameplay = config["Gameplay"];
            res_x = j_gameplay["Resolution"][0];
            res_y = j_gameplay["Resolution"][1];
            fps = j_gameplay["FPS"];
            if (res_x < 1 or res_y < 1 or fps < 1) throw exception::DomainError(message_start + "\"Gameplay\" should only contain positive integers greater than zero\"");
        } catch (std::domain_error& d){
            throw exception::DomainError(message_start + "\"Gameplay\" should only contain positive integers greater than zero");
        }

        //read all textures
        try {
            json j_textures = config["Textures"];
            textures.ship1 = j_textures["Ship1"];
            textures.ship2 = j_textures["Ship2"];
            textures.normal_enemy = j_textures["Normal_Enemy"];
            textures.quick_enemy = j_textures["Quick_Enemy"];
            textures.homing_enemy = j_textures["Homing_Enemy"];
            textures.laser_enemy = j_textures["Laser_Enemy"];
            textures.force_enemy = j_textures["Force_Enemy"];
            textures.background = j_textures["Background"];
            textures.friendly_bullet = j_textures["Friendly_Bullet"];
            textures.normal_enemy_bullet = j_textures["Normal_Enemy_Bullet"];
            textures.homing_bullet = j_textures["Homing_Bullet"];
            textures.laser_bullet = j_textures["Laser_Bullet"];
            textures.force_bullet = j_textures["Force_Bullet"];
            textures.force_field = j_textures["Force_Field"];
            textures.terrain1 = j_textures["Terrain1"];
            textures.terrain2 = j_textures["Terrain2"];
            textures.terrain3 = j_textures["Terrain3"];
            textures.obstacle1 = j_textures["Obstacle1"];
            textures.obstacle2 = j_textures["Obstacle2"];
            textures.obstacle3 = j_textures["Obstacle3"];
            textures.planet1 = j_textures["Planet1"];
            textures.planet2 = j_textures["Planet2"];
            textures.planet3 = j_textures["Planet3"];
        } catch (std::exception& e) {
            throw exception::DomainError(message_start + "\"Textures\" should only contain strings");
        }

        //read all the entity velocities
        try {
            json j_speeds = config["Speeds"];
            speeds.player = j_speeds["Player"];
            speeds.normal_enemy = j_speeds["Normal_Enemy"];
            speeds.quick_enemy = j_speeds["Quick_Enemy"];
            speeds.homing_enemy = j_speeds["Homing_Enemy"];
            speeds.laser_enemy = j_speeds["Laser_Enemy"];
            speeds.force_enemy = j_speeds["Force_Enemy"];
            speeds.friendly_bullet = j_speeds["Friendly_Bullet"];
            speeds.normal_enemy_bullet = j_speeds["Normal_Enemy_Bullet"];
            speeds.homing_bullet = j_speeds["Homing_Bullet"];
            speeds.laser_bullet = j_speeds["Laser_Bullet"];
            speeds.force_bullet = j_speeds["Force_Bullet"];
            speeds.force_field = j_speeds["Force_Field"];
            speeds.terrain = j_speeds["Terrain"];
            speeds.world = j_speeds["World"];
            speeds.background = j_speeds["Background"];
            speeds.planet = j_speeds["Planet"];
            speeds.obstacle = j_speeds["Obstacle"];
            if (speeds.player <= 0 or speeds.normal_enemy <= 0 or speeds.quick_enemy <= 0 or speeds.homing_enemy <= 0 or speeds.laser_enemy <= 0
            or speeds.force_enemy <= 0 or speeds.friendly_bullet <= 0 or speeds.normal_enemy_bullet <= 0 or speeds.homing_bullet <= 0
            or speeds.laser_bullet <= 0 or speeds.force_bullet <= 0 or speeds.force_field <= 0 or speeds.terrain <= 0 or speeds.world <= 0
            or speeds.background <= 0 or speeds.planet <= 0 or speeds.obstacle <= 0) {
                throw exception::DomainError(message_start + "\"Speeds\" should only contain positive real numbers");
            }
        } catch (std::domain_error& d){
            throw exception::DomainError(message_start + "\"Speeds\" should only contain positive real numbers");
        }

        try {
            //read all the lives for the entities
            json j_lives = config["Lives"];
            lives.player = j_lives["Player"];
            lives.normal_enemy = j_lives["Normal_Enemy"];
            lives.quick_enemy = j_lives["Quick_Enemy"];
            lives.homing_enemy = j_lives["Homing_Enemy"];
            lives.laser_enemy = j_lives["Laser_Enemy"];
            lives.force_enemy = j_lives["Force_Enemy"];
            lives.force_field = j_lives["Force_Field"];
            lives.obstacle = j_lives["Obstacle"];   
            if (lives.player <= 0 or lives.normal_enemy <= 0 or lives.quick_enemy <= 0 or lives.homing_enemy <= 0
            or lives.laser_enemy <= 0 or lives.force_enemy <= 0 or lives.force_field <= 0 or lives.obstacle <= 0) {
                throw exception::DomainError(message_start + "\"Lives\" should only contain positive integers");
            }
        } catch (std::domain_error& d) {
            throw exception::DomainError(message_start + "\"Lives\" should only contain positive integers");
        }

        try {
            json j_bullet = config["Bullet_Rates"];
            bullet_rates.player = j_bullet["Player"];
            bullet_rates.normal_enemy = j_bullet["Normal_Enemy"];
            bullet_rates.homing_enemy = j_bullet["Homing_Enemy"];
            bullet_rates.laser_enemy = j_bullet["Laser_Enemy"];
            bullet_rates.force_enemy = j_bullet["Force_Enemy"];
            if (bullet_rates.player <= 0 or bullet_rates.normal_enemy <= 0 or bullet_rates.homing_enemy <= 0
            or bullet_rates.laser_enemy <= 0 or bullet_rates.force_enemy <= 0) {
                throw exception::DomainError(message_start + "\"Bullet_Rates\" should only contain positive real numbers");
            }
        } catch (std::domain_error& d) {
            throw exception::DomainError(message_start + "\"Bullet_Rates\" should only contain positive real numbers");
        }

        try {
            //read all the hitbox sizes for the entities
            json j_hitbox = config["Hitbox"];
            hitboxes.player = j_hitbox["Player"];
            hitboxes.normal_enemy = j_hitbox["Normal_Enemy"];
            hitboxes.quick_enemy = j_hitbox["Quick_Enemy"];
            hitboxes.homing_enemy = j_hitbox["Homing_Enemy"];
            hitboxes.laser_enemy = j_hitbox["Laser_Enemy"];
            hitboxes.force_enemy = j_hitbox["Force_Enemy"];
            hitboxes.friendly_bullet = j_hitbox["Friendly_Bullet"];
            hitboxes.normal_enemy_bullet = j_hitbox["Normal_Enemy_Bullet"];
            hitboxes.homing_bullet = j_hitbox["Homing_Bullet"];
            hitboxes.laser_bullet = j_hitbox["Laser_Bullet"];
            hitboxes.force_bullet = j_hitbox["Force_Bullet"];
            hitboxes.force_field = j_hitbox["Force_Field"];
            hitboxes.terrain = j_hitbox["Terrain"];
            hitboxes.loop = j_hitbox["Loop"];
            hitboxes.obstacle = j_hitbox["Obstacle"];
            if (hitboxes.player <= 0 or hitboxes.normal_enemy <= 0 or hitboxes.quick_enemy <= 0 or hitboxes.homing_enemy <= 0 or hitboxes.laser_enemy <= 0
            or hitboxes.force_enemy <= 0 or hitboxes.friendly_bullet <= 0 or hitboxes.normal_enemy_bullet <= 0 or hitboxes.homing_bullet <= 0
            or hitboxes.laser_bullet <= 0 or hitboxes.force_bullet <= 0 or hitboxes.force_field <= 0 or hitboxes.terrain <= 0 or hitboxes.obstacle <= 0) {
                throw exception::DomainError(message_start + "\"Hitbox\" should only contain positive real numbers");
            }
        } catch (std::domain_error& d) {
            throw exception::DomainError(message_start + "\"Hitbox\" should only contain positive real numbers");
        }
    }

}

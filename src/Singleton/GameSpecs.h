//
// Created by atisha on 7/9/18.
//

#ifndef GP_PROJECT_GAMESPECS_H
#define GP_PROJECT_GAMESPECS_H

#include "Singleton.h"
#include <string>
#include <vector>
using std::string;

namespace singleton {

    /**
     * @brief struct that contains 'magic numbers' and game specs for the game
     */
    struct GameSpecs : public Singleton<GameSpecs> {

        friend class Singleton<GameSpecs>;

    public:

        struct Textures {
            Textures() = default;
            string ship1;
            string ship2;
            string normal_enemy;
            string quick_enemy;
            string homing_enemy;
            string laser_enemy;
            string force_enemy;
            string background;
            string friendly_bullet;
            string normal_enemy_bullet;
            string homing_bullet;
            string laser_bullet;
            string force_bullet;
            string force_field;
            string terrain1;
            string terrain2;
            string terrain3;
            string obstacle1;
            string obstacle2;
            string obstacle3;
            string planet1;
            string planet2;
            string planet3;
        };

        struct Speeds {
            Speeds() = default;
            float player;
            float normal_enemy;
            float quick_enemy;
            float homing_enemy;
            float laser_enemy;
            float force_enemy;
            float friendly_bullet;
            float normal_enemy_bullet;
            float homing_bullet;
            float laser_bullet;
            float force_bullet;
            float force_field;
            float terrain;
            float world;
            float background;
            float planet;
            float obstacle;
        };

        struct Lives {
            Lives() = default;
            int player;
            int normal_enemy;
            int quick_enemy;
            int homing_enemy;
            int laser_enemy;
            int force_enemy;
            int force_field;
            int obstacle;
        };

        struct BulletRates {
            BulletRates() = default;
            double player;
            double normal_enemy;
            double homing_enemy;
            double laser_enemy;
            double force_enemy;
        };

        struct Hitbox {
            Hitbox() = default;
            float player;
            float normal_enemy;
            float quick_enemy;
            float homing_enemy;
            float laser_enemy;
            float force_enemy;
            float friendly_bullet;
            float normal_enemy_bullet;
            float homing_bullet;
            float laser_bullet;
            float force_bullet;
            float force_field;
            float terrain;
            float loop;
            float obstacle;
        };

        Textures textures;
        Speeds speeds;
        Lives lives;
        BulletRates bullet_rates;
        Hitbox hitboxes;

        std::vector<string> level_files;
        string highscores = "";
        string key_bindings = "";
        string text_font = "";
        string texures_location = "";

        int res_x = 0;
        int res_y = 0;
        int fps = 0;

        /**
         * @brief initializes all variables in this struct by reading the json file
         * @param json_file the name of the config file
         */
        void initialize(string json_file);

    private:

        GameSpecs() = default;

    };

}

#endif //GP_PROJECT_GAMESPECS_H

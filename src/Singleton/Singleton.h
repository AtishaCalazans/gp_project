//
// Created by atisha on 06.12.17.
//

#ifndef GP_PROJECT_SINGLETON_H
#define GP_PROJECT_SINGLETON_H

#include <memory>

/**
 * @brief namespace that contains all singleton classes
 */
namespace singleton {

    template <class T>

    /**
    * @brief class that allows only one object to be made of a certain class
    * @tparam T singleton class
    */
    class Singleton {
    public:

        Singleton() = default;

        ~Singleton() = default;

        /**
         * @brief copy constructor is not allowed
         */
        Singleton(const Singleton&) = delete;

        /**
         * @brief copying the object is not allowed
         */
        Singleton& operator=(const Singleton&) = delete;

        /**
         * @return a refference to the single instance of the object
         */
        static T& get_instance() {

            static T instance;
            return instance;

        }

    };

}

#endif //GP_PROJECT_SINGLETON_H

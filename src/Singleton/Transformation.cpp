//
// Created by atisha on 16.12.17.
//

#include "Transformation.h"

namespace singleton {

    Transformation::Transformation() {
        m_x_scale = 1;
        m_y_scale = 1;
    }

    void Transformation::change_dimensions(const int& x, const int& y) {
        m_x_scale = x / 8;
        m_y_scale = y / 6;
    }

    float Transformation::get_x(const float& x) const{
        return (x + 4) * m_x_scale;
    }

    float Transformation::get_y(const float& y) const{
        return (y + 3) * m_y_scale;
    }

    float Transformation::get_scale() const{
        return m_x_scale;
    }

}
//
// Created by atisha on 03.12.17.
//

#ifndef GP_PROJECT_STOPWATCH_H
#define GP_PROJECT_STOPWATCH_H

#include "Singleton.h"
#include <chrono>

namespace singleton {

    /**
    * @brief class that is used to make every frame last an equal amount of time
    */
    class Stopwatch : public Singleton<Stopwatch> {

        friend class Singleton<Stopwatch>;

    public:

        /**
         * @brief start the timer
         */
        void start();

        /**
         * @brief if the required running time has not been reached, let the clock wait
         */
        void fill_remaining_time();

        /**
         * @brief change the time every frame takes up
         * @param fps new fps
         */
        void set_fps(const int& fps);

        /**
         * @return the amount of fps the clock is set to work with
         */
        int get_fps();

    private:

        std::chrono::system_clock::time_point m_start;
        float m_frame_duration;
        int m_fps;

        Stopwatch();

    };

}

#endif //GP_PROJECT_STOPWATCH_H

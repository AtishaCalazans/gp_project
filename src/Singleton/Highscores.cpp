//
// Created by atisha on 8/22/18.
//
#include "Highscores.h"
#include "GameSpecs.h"
#include "json.hpp"
#include "BaseException.h"
#include <fstream>

using nlohmann::json;

namespace singleton {

    void Highscores::initialize() {
        std::string json_file = GameSpecs::get_instance().highscores;
        json score_file;
        try {
            std::ifstream file(json_file);
            file >> score_file;
        } catch (std::exception& e) {
            throw exception::FileReadingError("Unable to read file \"" + json_file + "\"");
        }

        // fill in the top 10 list
        std::vector<std::string> names;
        std::vector<int> scores;
        std::string message_start = "In config file " + json_file + ": ";
        for (const std::string& name: score_file["Names"]) {
            if (name.empty() or name.size() > 6) throw exception::DomainError(message_start + "strings in \"Names\" should be 1 to 6 characters long");
            names.push_back(name);
        }
        for (const int& score: score_file["Scores"]) {
            if (score < 1) throw exception::DomainError(message_start + "integers in \"Scores\" should a positive integers");
            scores.push_back(score);
        }
        for (int i = 0; i < 10; ++i) {
            m_top_10.push_back({names[i], scores[i]});
        }
    }

    void Highscores::save_score(const std::string &name) {
        //spot for the score in the top_10 list
        int spot = 0;
        for (auto score: m_top_10) {
            if (m_current_score > std::get<1>(score)) break;
            ++spot;
        }
        if (spot < 10) {
            for (int i = 9; i >= spot+1; --i) {
                m_top_10[i] = m_top_10[i-1];
            }
            m_top_10[spot] = {name, m_current_score};
        }
    }

    void Highscores::add_to_score(const int &score) {
        m_current_score += score;
        if (m_current_score < 0) m_current_score = 0;
    }

    void Highscores::write_to_file() const {
        std::string json_file = GameSpecs::get_instance().highscores;
        json score_file;

        // fill in the top 10 list
        std::vector<std::string> names;
        std::vector<int> scores;
        for (const auto& tuple: m_top_10) {
            names.push_back(std::get<0>(tuple));
            scores.push_back(std::get<1>(tuple));
        }
        score_file["Names"] = names;
        score_file["Scores"] = scores;

        try {
            std::ofstream file(json_file);
            file << score_file;
        } catch (std::exception& e) {
            throw exception::FileReadingError("Unable to create file \"" + json_file + "\"");
        }

    }

    void Highscores::lock_fallback_score() {
        m_fallback_score = m_current_score;
    }

    void Highscores::fallback() {
        m_current_score = m_fallback_score;
    }

}
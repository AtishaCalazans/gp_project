//
// Created by atisha on 15.12.17.
//

#ifndef GP_PROJECT_TRANSFORMATION_H
#define GP_PROJECT_TRANSFORMATION_H

#include "Singleton.h"

namespace singleton {

    /**
    * @brief class that transforms coordinates from the model coordinate system to the sfml coordinate system
    */
    class Transformation: public Singleton<Transformation> {

        friend class Singleton<Transformation>;

    public:

        /**
         * @brief change the dimensions of the screen
         * @param x width of the screen
         * @param y height of the screen
         */
        void change_dimensions(const int& x, const int& y);

        /**
         * @param x model coordinate x value
         * @return sfml coordinate x value
         */
        float get_x(const float& x) const;

        /**
         * @param x model coordinate y value
         * @return sfml coordinate y value
         */
        float get_y(const float& y) const;

        /**
         * @return the ratio between the sfml coordinates and the model coordinates
         */
        float get_scale() const;

    private:

        float m_x_scale;
        float m_y_scale;

        Transformation();

    };

}

#endif //GP_PROJECT_TRANSFORMATION_H

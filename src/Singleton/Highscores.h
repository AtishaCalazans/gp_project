//
// Created by atisha on 8/22/18.
//

#ifndef GP_PROJECT_HIGHSCORES_H
#define GP_PROJECT_HIGHSCORES_H

#include "Singleton.h"
#include <vector>

namespace singleton {

    /**
     * @brief class used to read/save highscores and keep track of the current score
     */
    class Highscores: public Singleton<Highscores> {

        friend class Singleton<Highscores>;

    public:

        /**
         * @brief initialize the class by reading the highscores from a file
         */
        void initialize();

        /**
         * @brief save the current score in the top 10 with the given name
         */
        void save_score(const std::string& name);

        /**
         * @brief add points to the current score, if the score goes below 0, it's set to 0
         * @param score positive or negative integer
         */
        void add_to_score(const int& score);

        /**
         * @brief write the current top 10 to a file (saving the highscores)
         */
        void write_to_file() const;

        /**
         * @brief save the current score in a separate variable so that the current score can be reset to that value
         * Used at the beginning of a level
         */
        void lock_fallback_score();

        /**
         * @brief set the current score to the fallback score, this is done when the player dies
         */
        void fallback();

        int m_current_score = 0;
        std::vector<std::tuple<std::string, int>> m_top_10;

    private:

        int m_fallback_score = 0; //score at the beginning of the level

        Highscores() = default;

    };

}

#endif //GP_PROJECT_HIGHSCORES_H

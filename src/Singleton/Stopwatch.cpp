//
// Created by atisha on 03.12.17.
//
#include "Stopwatch.h"
#include <iostream>

namespace singleton {

    Stopwatch::Stopwatch() {
        //default fps is 120
        m_fps = 120;
        m_frame_duration = 1.0 / m_fps;
    }

    void Stopwatch::start() {
        m_start = std::chrono::system_clock::now();
    }

    void Stopwatch::fill_remaining_time() {
        std::chrono::duration<float> frame_duration = std::chrono::system_clock::now() - m_start;
        while (m_frame_duration > frame_duration.count()) {
            frame_duration = std::chrono::system_clock::now() - m_start;
        }
    }

    void Stopwatch::set_fps(const int& fps) {
        m_fps = fps;
        m_frame_duration = 1.0 / m_fps;
    }

    int Stopwatch::get_fps() {
        return m_fps;
    }

}


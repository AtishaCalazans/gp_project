//
// Created by atisha on 05.12.17.
//

#include "Controller.h"
#include "Model.h"
#include "MenuModel.h"
#include "PauseModel.h"
#include "HighscoreModel.h"
#include "KeybindingModel.h"
#include "Game.h"
#include "Stopwatch.h"
#include "GameSpecs.h"
#include "Highscores.h"
#include "Keybindings.h"
#include "json.hpp"
#include "BaseException.h"
#include <iostream>
#include <vector>
#include <fstream>
#include <stdlib.h>
#include <time.h>
#include <Highscores.h>

using nlohmann::json;

namespace controller {

    Controller::Controller(Game* game, std::shared_ptr<logic::GameModel>& model) {
        m_game = game;
        m_game_model = model;
        std::shared_ptr<draw::View> view = m_game->get_view();
        //start the game with the menu
        std::shared_ptr<logic::Model> current_model(new logic::MenuModel(view));
        m_current_model = current_model;
        m_current_model->initialize_entities();

        //set units the controller travels per frame
        //this is independent of the speed of the player
        m_units_per_frame = 5.0f / singleton::Stopwatch::get_instance().get_fps();
        m_obstacle_counter = 1;

        //start at the first level
        m_level_count = 0;
        //amount of time the game waits after killing the last enemy before transitioning to the next level
        m_level_end_wait = singleton::Stopwatch::get_instance().get_fps() * 2;
    }

    void Controller::evaluate_event(const sf::Event &event) {

        switch (event.type) {

            case sf::Event::Closed:
                m_game->close_window();
                break;

            case sf::Event::LostFocus:
                //pause the game
                if (m_current_status == logic::playing) {
                    auto view = m_game->get_view();
                    m_current_model = std::make_shared<logic::PauseModel>(view);
                    m_current_model->initialize_entities();
                    m_game_model->switch_pause(true);
                    m_current_status = logic::pause;
                }
                break;

            case sf::Event::KeyPressed:
                evaluate_key(event, true);
                break;

            case sf::Event::KeyReleased:
                evaluate_key(event, false);
                break;

            default:
                break;

        }
    }

    void Controller::evaluate_key(const sf::Event &event, const bool& pressed) {

        action current_action = Keybindings::get_instance()[event.key.code];

        //notify model of the input key string
        //this is for models that have to know the exact key that was pressed e.g. HighscoreModel when user input is required
        if (pressed) {
            m_key_input_str = Keybindings::get_instance().get_letter_input(event.key.code);
            if (!m_key_input_str.empty()) m_current_model->key_string_pressed(m_key_input_str);
            m_current_model->key_pressed(event.key.code);
        }

        switch (current_action) {

            case action::move_up_1:
                m_up_pressed_1 = pressed;
                break;

            case action::move_down_1:
                m_down_pressed_1 = pressed;
                break;

            case action::move_left_1:
                m_left_pressed_1 = pressed;
                break;

            case action::move_right_1:
                m_right_pressed_1 = pressed;
                break;

            case action::action_1:
                m_action_pressed_1 = pressed;
                break;

            case action::move_up_2:
                m_up_pressed_2 = pressed;
                break;

            case action::move_down_2:
                m_down_pressed_2 = pressed;
                break;

            case action::move_left_2:
                m_left_pressed_2 = pressed;
                break;

            case action::move_right_2:
                m_right_pressed_2 = pressed;
                break;

            case action::action_2:
                m_action_pressed_2 = pressed;
                break;

            case action::pause:
                //pause the game
                if (m_current_status == logic::playing) {
                    auto view = m_game->get_view();
                    m_current_model = std::make_shared<logic::PauseModel>(view);
                    m_current_model->initialize_entities();
                    m_game_model->switch_pause(true);
                    m_current_status = logic::pause;
                }
                break;

            case action::debug:
                if(pressed){m_game_model->switch_debug();}

            default:
                break;

        }
    }

    void Controller::notify_model() {
        srand(time(NULL));
        //check if the current model has to be replaced with a new one
        check_model_status();

        if (m_current_status == logic::playing or m_current_status == logic::next_level) {
            check_level_end();
            increase_counters();
        }

        update_keys();
        
        if (m_current_status == logic::playing) {
            terrain_generation();
            enemy_spawning();
            obstacle_generation();
        }

        m_current_model->tick();
    }

    void Controller::check_level_end() {
        if (m_current_pos > m_level_end) {
            m_game_model->end_level();
            //if model was able to finish the game
            if (m_game_model->get_game_status() == logic::next_level) {
                if (m_level_end_wait > 0) {
                    m_level_end_wait -= 1;
                } else {
                    //delete all entities and textures
                    flush(true);
                    ++m_level_count;

                    //load new level if necessary
                    if (m_level_count == singleton::GameSpecs::get_instance().level_files.size()) {
                        auto view = m_game->get_view();
                        m_current_model = std::make_shared<logic::HighscoreModel>(view);
                        m_current_model->initialize_entities();
                    } else {
                        load_next_level();
                        m_level_end_wait = singleton::Stopwatch::get_instance().get_fps() * 2;
                    }
                }
            }
        }
    }

    void Controller::increase_counters() {
        m_current_pos += m_units_per_frame;

        //an obstacle can be created every second;
        if (m_obstacle_counter == singleton::Stopwatch::get_instance().get_fps()) {m_obstacle_counter = 0;}
        else {m_obstacle_counter += 1;}
    }

    void Controller::update_keys() {
        //player one
        if (m_up_pressed_1) {
            m_current_model->move_up(true);
        }
        if (m_down_pressed_1) {
            m_current_model->move_down(true);
        }
        if (m_left_pressed_1) {
            m_current_model->move_left(true);
        }
        if (m_right_pressed_1) {
            m_current_model->move_right(true);
        }
        if (m_action_pressed_1) {
            m_current_model->action(true);
        }
        //player two
        if (m_up_pressed_2) {
            m_current_model->move_up(false);
        }
        if (m_down_pressed_2) {
            m_current_model->move_down(false);
        }
        if (m_left_pressed_2) {
            m_current_model->move_left(false);
        }
        if (m_right_pressed_2) {
            m_current_model->move_right(false);
        }
        if (m_action_pressed_2) {
            m_current_model->action(false);
        }
    }

    void Controller::terrain_generation() {
        //only keep looking for terrain to create if there is still terrain to be created
        if (m_terrain_index < m_terrain_spawns.size()) {

            bool found_new_terrain = true;

            //while there is new terrain to be generated for the current position
            while (found_new_terrain) {
                found_new_terrain = false;
                Terrain_Info next_terrain = m_terrain_spawns[m_terrain_index];

                //if we just passed a spawn point, create terrain
                if (m_current_pos > next_terrain.loc) {
                    m_game_model->create_terrain(next_terrain.height, next_terrain.length, next_terrain.bot);
                    //set index to look at the next terrain creation
                    ++m_terrain_index;
                }

            }

        }
    }

    void Controller::enemy_spawning() {
        //only keep looking for enemies to spawn if there are still enemies to be spawned
        if (m_enemy_index < m_enemy_spawns.size()) {

            bool found_new_wave = true;

            //while there are new enemies to be spawned for the current position
            while (found_new_wave) {
                found_new_wave = false;
                Enemy_Info next_wave = m_enemy_spawns[m_enemy_index];

                //if we just passed a spawn point, spawn enemies
                if (m_current_pos > next_wave.loc) {
                    for (int i = 0; i < next_wave.amount; ++i) {
                        m_game_model->create_enemy(next_wave.type, -2 + ((rand() % 9) * 0.5));
                    }
                    //set index to look at the next terrain creation
                    ++m_enemy_index;
                }

            }

        }
    }

    void Controller::obstacle_generation() {
        if (m_obstacle_counter == 0) {
            //an obstacle gets created on average every 6 / 3 / 2 seconds (changes with level)
            int obstacle_creation = rand() % (6 / (m_level_count + 1));

            if (obstacle_creation == (6 / (m_level_count + 1)) - 1) {
                //let the y value vary
                m_game_model->create_obstacle(-2 + ((rand() % 9) * 0.5));
            }
        }
    }

    void Controller::load_next_level() {

        flush(true);

        //=============================== LOAD THE FILE ================================//

        json level;
        std::string level_name = "../Levels/" + singleton::GameSpecs::get_instance().level_files[m_level_count];
        try {
            std::ifstream file(level_name);
            file >> level;
        } catch (std::exception& e) {
            throw exception::FileReadingError("Unable to read file \"" + level_name + "\"");
        }

        //let model read the textures
        m_game_model->load_level(level);

        //=============================== READ BASIC LEVEL INFO ================================//

        m_level_end = level["Level_end"];
        if (m_level_end <= 0) throw exception::DomainError("\"Level_end\" has to be a number greater than 0");

        //=============================== READ TERRAIN GENERATION INFO ================================//

        std::vector<json> terrain = level["Terrain"];
        for (json& spawn: terrain) {
            Terrain_Info new_spawn = Terrain_Info();
            new_spawn.bot = spawn["Bot"];
            new_spawn.loc = spawn["Loc"];
            if (new_spawn.loc <= 0 or new_spawn.loc >= m_level_end) {
                throw exception::DomainError("\"Loc\" has to be a number greater than 0 and smaller than Level_end");
            }
            new_spawn.height = spawn["Height"];
            if (new_spawn.height < 1 or new_spawn.height > 9) {
                throw exception::DomainError("\"Height\" has to be a number from 1 to 9");
            }
            //TODO check if this needs an exception
            new_spawn.length = spawn["Width"];
            m_terrain_spawns.push_back(new_spawn);
        }

        //=============================== READ ENEMY SPAWNING INFO ================================//

        std::vector<json> enemies = level["Enemies"];
        for (json& spawn: enemies) {
            Enemy_Info new_spawn = Enemy_Info();
            new_spawn.loc = spawn["Loc"];
            if (new_spawn.loc <= 0 or new_spawn.loc >= m_level_end) {
                throw exception::DomainError("\"Loc\" has to be a number greater than 0 and smaller than \"Level_end\"");
            }
            new_spawn.amount = spawn["Amount"];
            if (new_spawn.amount < 1) throw exception::DomainError("\"Amount\" has to be greater than 0");
            int type = spawn["Type"];

            switch (type) {
                case 0:
                    new_spawn.type = logic::normal;
                    break;

                case 1:
                    new_spawn.type = logic::quick;
                    break;

                case 2:
                    new_spawn.type = logic::laser;
                    break;

                case 3:
                    new_spawn.type = logic::homing;
                    break;

                case 4:
                    new_spawn.type = logic::force;
                    break;

                default:
                    throw exception::DomainError("\"Enemy\" type has to be a number from 0 to 4");
            }
            m_enemy_spawns.push_back(new_spawn);
        }
    }

    void Controller::flush(bool hard_flush) {
        //reset everything;
        m_obstacle_counter = 1;
        m_current_pos = 0;
        m_terrain_index = 0;
        m_enemy_index = 0;
        if (hard_flush) {
            m_terrain_spawns.clear();
            m_enemy_spawns.clear();
        }
        m_game_model->flush(hard_flush);
    }

    void Controller::check_model_status() {
        //if the game status has changed
        if(m_current_model->get_game_status() != m_current_status) {

            m_current_status = m_current_model->get_game_status();
            auto view = m_game->get_view();

            switch (m_current_status) {

                case logic::exit:
                    m_game->close_window();
                    break;

                case logic::game_over:
                    //the player has died, restart the level
                    flush(false);
                    m_game_model->initialize_entities();
                    break;

                case logic::start_singleplayer:
                    singleton::Highscores::get_instance().m_current_score = 0;
                    m_current_model = m_game_model;
                    m_game_model->set_multiplayer(false);
                    load_next_level();
                    break;

                case logic::start_multiplayer:
                    singleton::Highscores::get_instance().m_current_score = 0;
                    m_current_model = m_game_model;
                    m_game_model->set_multiplayer(true);
                    load_next_level();
                    break;
                    
                case logic::playing:
                    //the game is running
                    m_current_model = m_game_model;
                    m_game_model->switch_pause(false);
                    break;
                    
                case logic::highscores:
                    m_current_model = std::make_shared<logic::HighscoreModel>(view);
                    m_current_model->initialize_entities();
                    break;
                
                case logic::keybindings:
                    //currently in the keybindings menu
                    m_current_model = std::make_shared<logic::KeybindingModel>(view);
                    m_current_model->initialize_entities();
                    break;

                case logic::menu:
                    //currently in the start menu of the game
                    flush(true);
                    m_level_count = 0;
                    singleton::Highscores::get_instance().m_current_score = 0;
                    m_current_model = std::make_shared<logic::MenuModel>(view);
                    m_current_model->initialize_entities();
                    break;

                case logic::pause:
                    //currently the game is paused
                    m_current_model = std::make_shared<logic::PauseModel>(view);
                    m_current_model->initialize_entities();
                    m_game_model->switch_pause(true);
                    break;

		        case logic::next_level:
		            break;

                default:
                    throw exception::EnumDomainError("Invalid game state");
            }
        }
    }

}

//
// Created by atisha on 03.12.17.
//

#ifndef GP_PROJECT_CONTROLLER_H
#define GP_PROJECT_CONTROLLER_H

#include <SFML/Window/Event.hpp>
#include <memory>
#include <vector>
#include <GameModel.h>
#include "Enemies/EnemyFactory.h"
#include "json.hpp"

class Game;
namespace logic {class Model; enum game_status;}

/**
 * @brief namespace that contains everything that handles input and the creation of enemies, terrain and objects
 */
namespace controller {

    /**
     * @brief struct used for the creation of terrain
     */
    struct Terrain_Info {
        bool bot;
        int loc;
        int height;
        int length;
    };

    /**
     * @brief struct used for the spawning of enemies
     */
    struct Enemy_Info {
        logic::enemy_type type;
        int loc;
        int amount;
    };

    /**
     * @brief class that handles input and creation of terrain, enemies and obstacles. Communicates with Model
     */
    class Controller {
    public:

        Controller(Game* game, std::shared_ptr<logic::GameModel>& model);

        /**
         * @brief evaluates an sfml event
         * @param event event to be evaluated
         */
        void evaluate_event(const sf::Event& event);

        /**
         * @brief evaluates an key event
         * @param event key event
         * @param pressed true if key is being pressed, false if released
         */
        void evaluate_key(const sf::Event& event, const bool& pressed);

        /**
         * @brief lets Model know if things have to be created and if the player has to move
         */
        void notify_model();

        /**
         * @brief reads a json file to extract information about the level
         * @param level_name the name of a json file
         */
        void load_next_level();

        /**
         * @brief resets data and counters
         * @param hard_flush true if terrain_info and enemy_info has to be cleared as well
         */
        void flush(bool hard_flush);

        /**
         * @brief check the status of the model to see what needs to happen next
         * If the status changed, the current model is switched out for a new one
         */
        void check_model_status();

    private:

        Game* m_game;
        std::shared_ptr<logic::GameModel> m_game_model;
        std::shared_ptr<logic::Model> m_current_model;
        logic::game_status m_current_status = logic::menu;

        bool m_up_pressed_1 = false;
        bool m_down_pressed_1 = false;
        bool m_left_pressed_1 = false;
        bool m_right_pressed_1 = false;
        bool m_action_pressed_1 = false;
        bool m_up_pressed_2 = false;
        bool m_down_pressed_2 = false;
        bool m_left_pressed_2 = false;
        bool m_right_pressed_2 = false;
        bool m_action_pressed_2 = false;
        string m_key_input_str;

        float m_units_per_frame;
        float m_current_pos = 4; //always be 4 units ahead so terrain/enemies can spawn in time
        int m_terrain_index = 0; //index for the terrain vector
        int m_enemy_index = 0; //index for the enemy vector
        std::vector<Terrain_Info> m_terrain_spawns;
        std::vector<Enemy_Info> m_enemy_spawns;

        int m_obstacle_counter;

        int m_level_count;
        std::vector<std::string> m_level_files;
        int m_level_end;
        int m_level_end_wait;

        /**
         * @brief check if the player has reached the end of the level
         */
        void check_level_end();

        /**
         * @brief increase all counters
         */
        void increase_counters();

        /**
         * @brief notify the model what keys are pressed
         */
        void update_keys();

        /**
         * @brief generate the terrain for the level
         */
        void terrain_generation();

        /**
         * @brief spawn enemies
         */
        void enemy_spawning();

        /**
         * @brief generate obstacles
         */
        void obstacle_generation();

    };

}

#endif //GP_PROJECT_CONTROLLER_H

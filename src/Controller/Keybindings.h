//
// Created by atisha on 7/16/18.
//

#ifndef GP_PROJECT_KEYBINDINGS_H
#define GP_PROJECT_KEYBINDINGS_H

#include <memory>
#include <map>
#include <string>
#include <SFML/Window/Event.hpp>
#include "Singleton.h"

namespace controller {

    enum action {
        move_up_1,
        move_down_1,
        move_right_1,
        move_left_1,
        action_1,
        move_up_2,
        move_down_2,
        move_right_2,
        move_left_2,
        action_2,
        pause,
        debug,
        LAST
    };

    /**
     * @brief class used for translating the inputs to meaningful actions ingame
     */
class Keybindings: public singleton::Singleton<Keybindings> {

    friend class singleton::Singleton<Keybindings>;

    public:

        Keybindings();

        /**
         * @brief reads keybindings from file
         */
        void read_from_file();

        /**
         * @brief write keybindings to file
         */
         void write_to_file();

        /**
         * @brief set keybindings to default
         */
        void set_default();

        /**
         * @brief change the keybinding for the specified action
         * @param action action that needs to be changed
         * @param key new key that belongs to the action
         */
        void change_keybinding(const action action, const sf::Keyboard::Key key);

        /**
         * @param key key to be checked
         * @return action belonging to the specified key
         */
        action operator[](const sf::Keyboard::Key key);

        /**
         * @param action action to be checked
         * @return key belonging to the specified action
         */
        sf::Keyboard::Key operator[](const action action);

        /**
         * @brief gives a string representation of an action
         */
        std::string to_str(const action action);

        /**
         * @brief gives a string representation of an sfml key
         */
        std::string to_str(const sf::Keyboard::Key key);

        /**
         * @brief gives a string representation of a key being pressed only if it's a letter
         */
        std::string get_letter_input(const sf::Keyboard::Key key);

        /**
         * @brief returns the string representation of the key mapped to the given action
         */
        std::string get_str_action_key(const action action);

    private:

        //keybindings
        std::map<sf::Keyboard::Key, action> m_key_to_action;
        std::map<action, sf::Keyboard::Key> m_action_to_key;

        //string representation (mainly used for the keybinding menu)
        std::map<sf::Keyboard::Key, std::string> m_key_to_str;
        std::map<sf::Keyboard::Key, std::string> m_letter_key_to_str;
        std::map<action, std::string> m_action_to_str;

    };

}

#endif //GP_PROJECT_KEYBINDINGS_H

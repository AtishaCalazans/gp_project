//
// Created by atisha on 7/16/18.
//

#include "Keybindings.h"
#include "GameSpecs.h"
#include "json.hpp"
#include "BaseException.h"
#include <iostream>
#include <fstream>

using nlohmann::json;

namespace controller {

    Keybindings::Keybindings() {
        //set the maps
        m_action_to_str = {
                {move_up_1, "move_up_1"},
                {move_down_1, "move_down_1"},
                {move_right_1, "move_right_1"},
                {move_left_1, "move_left_1"},
                {action_1, "action_1"},
                {move_up_2, "move_up_2"},
                {move_down_2, "move_down_2"},
                {move_right_2, "move_right_2"},
                {move_left_2, "move_left_2"},
                {action_2, "action_2"},
                {pause, "pause"},
                {debug, "debug"},
                {LAST, "INVALID"}
        };

        m_key_to_str = {
                {sf::Keyboard::Num0, "Num0"},
                {sf::Keyboard::Num1, "Num1"},
                {sf::Keyboard::Num2, "Num2"},
                {sf::Keyboard::Num3, "Num3"},
                {sf::Keyboard::Num4, "Num4"},
                {sf::Keyboard::Num5, "Num5"},
                {sf::Keyboard::Num6, "Num6"},
                {sf::Keyboard::Num7, "Num7"},
                {sf::Keyboard::Num8, "Num8"},
                {sf::Keyboard::Num9, "Num9"},
                {sf::Keyboard::Numpad0, "NumP0"},
                {sf::Keyboard::Numpad1, "NumP1"},
                {sf::Keyboard::Numpad2, "NumP2"},
                {sf::Keyboard::Numpad3, "NumP3"},
                {sf::Keyboard::Numpad4, "NumP4"},
                {sf::Keyboard::Numpad5, "NumP5"},
                {sf::Keyboard::Numpad6, "NumP6"},
                {sf::Keyboard::Numpad7, "NumP7"},
                {sf::Keyboard::Numpad8, "NumP8"},
                {sf::Keyboard::Numpad9, "NumP9"},
                {sf::Keyboard::Escape, "ESC"},
                {sf::Keyboard::LControl, "L_CTRL"},
                {sf::Keyboard::RControl, "R_CTRL"},
                {sf::Keyboard::LShift, "L_SHIFT"},
                {sf::Keyboard::RShift, "R_SHIFT"},
                {sf::Keyboard::LAlt, "L_ALT"},
                {sf::Keyboard::RAlt, "R_ALT"},
                {sf::Keyboard::Space, "SPACE"},
                {sf::Keyboard::Return, "ENTER"},
                {sf::Keyboard::BackSpace, "BCKSPC"},
                {sf::Keyboard::Tab, "TAB"},
                {sf::Keyboard::Up, "UP"},
                {sf::Keyboard::Down, "DOWN"},
                {sf::Keyboard::Right, "RIGHT"},
                {sf::Keyboard::Left, "LEFT"}
        };

        m_letter_key_to_str = {
                {sf::Keyboard::A, "A"},
                {sf::Keyboard::B, "B"},
                {sf::Keyboard::C, "C"},
                {sf::Keyboard::D, "D"},
                {sf::Keyboard::E, "E"},
                {sf::Keyboard::F, "F"},
                {sf::Keyboard::G, "G"},
                {sf::Keyboard::H, "H"},
                {sf::Keyboard::I, "I"},
                {sf::Keyboard::J, "J"},
                {sf::Keyboard::K, "K"},
                {sf::Keyboard::L, "L"},
                {sf::Keyboard::M, "M"},
                {sf::Keyboard::N, "N"},
                {sf::Keyboard::O, "O"},
                {sf::Keyboard::P, "P"},
                {sf::Keyboard::Q, "Q"},
                {sf::Keyboard::R, "R"},
                {sf::Keyboard::S, "S"},
                {sf::Keyboard::T, "T"},
                {sf::Keyboard::U, "U"},
                {sf::Keyboard::V, "V"},
                {sf::Keyboard::W, "W"},
                {sf::Keyboard::X, "X"},
                {sf::Keyboard::Y, "Y"},
                {sf::Keyboard::Z, "Z"},
        };

        //initialize keybindings
        Keybindings::read_from_file();
    }

    void Keybindings::read_from_file(){
        std::string key_binding_file = singleton::GameSpecs::get_instance().key_bindings;
        json config;
        try {
            std::ifstream file(key_binding_file);
            file >> config;
        } catch (std::exception& e) {
            throw exception::FileReadingError("Unable to read file \"" + key_binding_file + "\"");
        }

        try {
            Keybindings::change_keybinding(move_up_1, config["move_up_1"]);
            Keybindings::change_keybinding(move_down_1, config["move_down_1"]);
            Keybindings::change_keybinding(move_right_1, config["move_right_1"]);
            Keybindings::change_keybinding(move_left_1, config["move_left_1"]);
            Keybindings::change_keybinding(action_1, config["action_1"]);

            Keybindings::change_keybinding(move_up_2, config["move_up_2"]);
            Keybindings::change_keybinding(move_down_2, config["move_down_2"]);
            Keybindings::change_keybinding(move_right_2, config["move_right_2"]);
            Keybindings::change_keybinding(move_left_2, config["move_left_2"]);
            Keybindings::change_keybinding(action_2, config["action_2"]);

            Keybindings::change_keybinding(pause, config["pause"]);
            Keybindings::change_keybinding(debug, config["debug"]);
        } catch (std::domain_error& d) {
            throw exception::DomainError("In " + key_binding_file + ": Key has to be specified by a positive integer");
        }

    }

    void Keybindings::write_to_file() {
        std::string key_binding_file = singleton::GameSpecs::get_instance().key_bindings;
        json config;

        //write everything to the json object
        for (int i = 0; i < LAST; ++i) {
            action current_action = static_cast<action>(i);
            config[m_action_to_str[current_action]] = m_action_to_key[current_action];
        }

        //write the json object to the file
        try {
            std::ofstream file(key_binding_file);
            file << config;
        } catch (std::exception& e) {
            throw exception::FileReadingError("Unable to open file \"" + key_binding_file + "\"");
        }
    }

    void Keybindings::set_default() {
        m_key_to_action.clear();
        m_action_to_key.clear();
        Keybindings::change_keybinding(move_up_1, sf::Keyboard::Z);
        Keybindings::change_keybinding(move_down_1, sf::Keyboard::S);
        Keybindings::change_keybinding(move_right_1, sf::Keyboard::D);
        Keybindings::change_keybinding(move_left_1, sf::Keyboard::Q);
        Keybindings::change_keybinding(action_1, sf::Keyboard::Space);

        Keybindings::change_keybinding(move_up_2, sf::Keyboard::Up);
        Keybindings::change_keybinding(move_down_2, sf::Keyboard::Down);
        Keybindings::change_keybinding(move_right_2, sf::Keyboard::Right);
        Keybindings::change_keybinding(move_left_2, sf::Keyboard::Left);
        Keybindings::change_keybinding(action_2, sf::Keyboard::RControl);

        Keybindings::change_keybinding(pause, sf::Keyboard::Escape);
        Keybindings::change_keybinding(debug, sf::Keyboard::B);

    }

    void Keybindings::change_keybinding(const action action, const sf::Keyboard::Key key) {
        try {
            //test if the key is supported
            to_str(key);
            if (m_key_to_action.count(key) != 0) throw exception::KeyBindingError("Key already bound to an action");

            // first erase the old keybinding
            sf::Keyboard::Key old_key = m_action_to_key[action];
            m_key_to_action.erase(old_key);
            // replace by new keybinding
            m_key_to_action[key] = action;
            m_action_to_key[action] = key;

        } catch (exception::KeyBindingError& k) {
            // do nothing
            // this exception is thrown when a key is already bound to an action so the call to this function should just do nothing
        }
    }

    action Keybindings::operator[](const sf::Keyboard::Key key) {
        if (m_key_to_action.count(key) != 0) {
            return m_key_to_action[key];
        } else {
            return LAST;
        }
    }

    sf::Keyboard::Key Keybindings::operator[](const action action) {
        return m_action_to_key[action];
    }
    
    std::string Keybindings::to_str(const sf::Keyboard::Key key) {
        if (m_key_to_str.count(key) != 0) {
            return m_key_to_str[key];
        } else if (m_letter_key_to_str.count(key) != 0){
            return m_letter_key_to_str[key];
        } else {
            throw exception::KeyBindingError("Key not found");
        }
    }

    std::string Keybindings::to_str(const controller::action action) {
        if (m_action_to_str.count(action) != 0) {
            return m_action_to_str[action];
        } else {
            throw exception::KeyBindingError("Action not found");
        }
    }

    std::string Keybindings::get_letter_input(const sf::Keyboard::Key key) {
        if (m_letter_key_to_str.count(key) != 0){
            return m_letter_key_to_str[key];
        } else {
            return "";
        }
    }

    std::string Keybindings::get_str_action_key(const controller::action action) {
        return to_str(m_action_to_key[action]);
    }

}
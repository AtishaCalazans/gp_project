//
// Created by atisha on 7/19/18.
//

#ifndef GP_PROJECT_TEXTVIEW_H
#define GP_PROJECT_TEXTVIEW_H

#include "Text.h"
#include "EntityView.h"
#include <memory>
#include <SFML/Graphics.hpp>

namespace draw {

    /**
     * @brief class that is associated with a Text object and contains the sprite for that object
     */
    class TextView : public EntityView{
    public:

        TextView(const std::shared_ptr<logic::Text>& text, const std::shared_ptr<sf::RenderWindow>& window);

        /**
         * @brief looks up the position, rotation, status and visibilty of the text
         */
        virtual void update();

        /**
         * @brief draws the text sprite to the render window
         */
        virtual void draw();

    protected:

        std::shared_ptr<logic::Text> m_text;
        std::unique_ptr<sf::Text> m_text_sprite;

        sf::Font m_font;
        float m_scale;

    };

}

#endif //GP_PROJECT_TEXTVIEW_H

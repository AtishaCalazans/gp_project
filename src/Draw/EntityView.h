//
// Created by atisha on 17.12.17.
//

#ifndef GP_PROJECT_ENTITYVIEW_H
#define GP_PROJECT_ENTITYVIEW_H

#include <memory>
#include <SFML/Graphics.hpp>
#include "Entity.h"

namespace draw {

    /**
     * @brief class that is associated with an entity and contains the sprite for that entity
     */
    class EntityView {
    public:

        EntityView() = default;

        EntityView(const std::shared_ptr<logic::Entity>& entity, const std::shared_ptr<sf::RenderWindow>& window);

        /**
         * @brief set the texture for the sprite
         * @param texture the texture that the sprite will use
         * @param x_size width of the sprite if the sprite needs a different width than the texture
         * @param y_size height of the sprite if the sprite needs a different height than the texture
         */
        void set_texture(const sf::Texture& texture, const int& x_size = 0, const int& y_size = 0);

        /**
         * @brief looks up the position, rotation and status of the entity
         */
        virtual void update();

        /**
         * @brief draws the sprite to the render window
         */
        virtual void draw();

        /**
         * @brief draws the hitbox of the entity
         * @param color the color the hitbox needs to have
         */
        void draw_hitbox(sf::Color color);

        /**
         * @return the status of the entity
         */
        logic::Status get_status() const;

        /**
         * @brief turn the sprite red if it was hit
         */
        void show_damage();

    protected:

        std::shared_ptr<sf::RenderWindow> m_render_window;
        std::shared_ptr<logic::Entity> m_entity;
        std::unique_ptr<sf::Sprite> m_sprite;
        logic::Status m_status = logic::alive;

        int m_damage_counter = 0;

    };

}

#endif //GP_PROJECT_ENTITYVIEW_H

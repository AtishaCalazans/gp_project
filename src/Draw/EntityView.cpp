//
// Created by atisha on 17.12.17.
//

#include "EntityView.h"
#include "Entity.h"
#include "Transformation.h"
#include <iostream>
#include <Stopwatch.h>

namespace draw {

    EntityView::EntityView(const std::shared_ptr<logic::Entity>& entity, const std::shared_ptr<sf::RenderWindow>& window) {
        m_render_window = window;
        m_entity = entity;
        std::unique_ptr<sf::Sprite> sprite(new sf::Sprite);
        m_sprite = std::move(sprite);

        float scale = singleton::Transformation::get_instance().get_scale() / 100.0f;
        m_sprite->scale(scale, scale);
    }

    void EntityView::set_texture(const sf::Texture& texture, const int& x_size, const int& y_size) {
        m_sprite->setTexture(texture);
        sf::FloatRect bound_rect = m_sprite->getLocalBounds();
        float sprite_size_x = bound_rect.width;
        float sprite_size_y = bound_rect.height;

        if (x_size >= 1) {sprite_size_x = x_size * singleton::Transformation::get_instance().get_scale();}
        if (y_size >= 1) {sprite_size_y = y_size * singleton::Transformation::get_instance().get_scale();}

        m_sprite->setTextureRect(sf::IntRect(0, 0, sprite_size_x, sprite_size_y));
        //getLocalBounds() has to be called again because the dimensions of the sprite have changed
        bound_rect = m_sprite->getLocalBounds();
        m_sprite->setOrigin(bound_rect.left + bound_rect.width/2.0f, bound_rect.top  + bound_rect.height/2.0f);
    }

    void EntityView::update() {
        float x = singleton::Transformation::get_instance().get_x(m_entity->get_x());
        float y = singleton::Transformation::get_instance().get_y(m_entity->get_y());
        m_sprite->setPosition(x, y);
        m_sprite->setRotation(m_entity->get_rotation());
        m_status = m_entity->get_status();
        if (m_status == logic::invulnerable) {
            m_sprite->setColor(sf::Color(255, 255, 255, 122));
        } else {
            int visibility = int(m_entity->get_visibility() * 255.0);
            m_sprite->setColor(sf::Color(255, 255, 255, visibility));
        }
    }

    void EntityView::draw() {
        if (m_damage_counter > 0) {
            m_damage_counter -= 1;
            if (m_damage_counter == 0) {
                m_sprite->setColor(sf::Color(0, 0, 0));
            }
            m_sprite->setColor(sf::Color(128, 0, 0));
        }
        m_render_window->draw(*m_sprite);
    }

    void EntityView::draw_hitbox(sf::Color color) {
        float radius = singleton::Transformation::get_instance().get_scale() * m_entity->get_hitbox_radius();
        sf::CircleShape shape(radius);

        //set circle origin
        sf::FloatRect bound_rect = shape.getLocalBounds();
        shape.setOrigin(bound_rect.left + bound_rect.width/2.0f, bound_rect.top  + bound_rect.height/2.0f);

        shape.setPosition(m_sprite->getPosition());
        shape.setOutlineColor(color);
        shape.setOutlineThickness(1);
        shape.setFillColor(sf::Color::Transparent);
        m_render_window->draw(shape);
    }

    logic::Status EntityView::get_status() const {
        return m_status;
    }

    void EntityView::show_damage() {
        m_damage_counter = singleton::Stopwatch::get_instance().get_fps() / 8.0;
    }

}
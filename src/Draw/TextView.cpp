//
// Created by atisha on 7/19/18.
//

#include "TextView.h"
#include "Text.h"
#include "Transformation.h"
#include "GameSpecs.h"
#include <memory>
#include <iostream>

namespace draw {

    TextView::TextView(const std::shared_ptr<logic::Text> &text, const std::shared_ptr<sf::RenderWindow> &window)
            : EntityView(text, window) {
        m_text = text;
        //text is scaled to match the screen resolution
        m_scale = singleton::Transformation::get_instance().get_scale() / 100.0f;
        std::unique_ptr<sf::Text> text_sprite(new sf::Text);
        m_text_sprite = std::move(text_sprite);

        //load font from file
        string filename = singleton::GameSpecs::get_instance().text_font;
        if (!m_font.loadFromFile(singleton::GameSpecs::get_instance().text_font)) {
            throw std::runtime_error("Couldn't load font");
        }
    }

    void TextView::update() {
        m_status = m_entity->get_status();
        m_text_sprite->setRotation(m_text->get_rotation());
        m_text_sprite->setFont(m_font);
        m_text_sprite->setString(m_text->get_text());
        m_text_sprite->setCharacterSize(m_text->get_char_size() * m_scale);
        m_text_sprite->setColor(sf::Color(255, 255, 255, m_text->get_visibility()));
        m_text_sprite->setOrigin(m_text_sprite->getGlobalBounds().width/2, m_text_sprite->getGlobalBounds().height/2);
        float x = singleton::Transformation::get_instance().get_x(m_text->get_x());
        float y = singleton::Transformation::get_instance().get_y(m_text->get_y());
        m_text_sprite->setPosition(x , y);
    }

    void TextView::draw() {
        m_render_window->draw(*m_text_sprite);
    }

}
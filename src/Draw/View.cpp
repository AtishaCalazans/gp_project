//
// Created by atisha on 09.12.17.
//

#include <Stopwatch.h>
#include <Background.h>
#include <Transformation.h>
#include "View.h"
#include "Entity.h"
#include "Text.h"
#include "Loop.h"
#include "EntityView.h"
#include "TextView.h"
#include "ButtonView.h"
#include "GameSpecs.h"
#include "BaseException.h"

namespace draw {

    View::View(const std::shared_ptr<sf::RenderWindow>& window) {
        m_textures_loc = singleton::GameSpecs::get_instance().texures_location;
        m_render_window = window;

        string font_file = singleton::GameSpecs::get_instance().text_font;
        if (!m_default_font.loadFromFile(font_file)) {
            throw exception::FileReadingError("Couldn't load font " + font_file);
        }
        //text is scaled to match the screen resolution
        float scale = singleton::Transformation::get_instance().get_scale() / 100.0f;
        m_entity_count.setFont(m_default_font);
        m_entity_count.setCharacterSize(15 * scale);
        m_entity_count.setPosition(10 * scale, m_render_window->getSize().y - 20 * scale);
        m_entity_count.setColor(sf::Color::White);
    }

    void View::load_textures(std::vector<std::string> &texture_names) {

        for (std::string& texture_name: texture_names) {
            sf::Texture new_texture;
            if (!new_texture.loadFromFile(m_textures_loc + texture_name)) {
                throw exception::FileReadingError("Couldn't load texture \"" + texture_name + "\"");
            }
            m_textures[texture_name] = new_texture;
        }

    }

    void View::add_entity(const std::shared_ptr<logic::Entity>& entity){
        //make an EntityView and give its corresponding entity and texture
        std::string texture_name = entity->get_texture_name();
        std::shared_ptr<EntityView> new_entity_view(new EntityView(entity, m_render_window));
        entity->add_observer(new_entity_view);

        //if the texture wasn't created
        if (m_textures.count(texture_name) == 0) throw exception::TextureNotLoadedError("Couldn't find texture: \"" + texture_name + "\"");

        new_entity_view->set_texture(m_textures[texture_name]);
        m_entity_views.push_back(new_entity_view);
    }

    void View::add_text(const std::shared_ptr<logic::Text> &text) {
        std::shared_ptr<TextView> new_text_view(new TextView(text, m_render_window));
        text->add_observer(new_text_view);
        m_text_views.push_back(new_text_view);
    }

    void View::add_button(const std::shared_ptr<logic::Button> &button) {
        std::shared_ptr<ButtonView> new_text_view(new ButtonView(button, m_render_window));
        button->add_observer(new_text_view);
        m_text_views.push_back(new_text_view);
    }

    void View::add_background(const std::shared_ptr<logic::Entity> &entity, const int& x_size, const int& y_size) {
        //make an EntityView and give its corresponding entity and texture
        std::string texture_name = entity->get_texture_name();
        std::shared_ptr<EntityView> new_entity_view(new EntityView(entity, m_render_window));
        entity->add_observer(new_entity_view);

        //if the texture wasn't created
        if (m_textures.count(texture_name) == 0) throw exception::TextureNotLoadedError("Couldn't find texture: \"" + texture_name + "\"");

        //since the sprite will be scaled (according to the screen resolution), x_size and y_size will change as well,
        //these don't need to be changed though, this is why it is scaled (with an inverted ratio) beforehand to undo
        //the scaling that will happen afterwards
        float prescale = 100.0f / singleton::Transformation::get_instance().get_scale();
        m_textures[texture_name].setRepeated(true);
        new_entity_view->set_texture(m_textures[texture_name], prescale * x_size, prescale * y_size);
        m_background.push_back(new_entity_view);
    }

    void View::draw() {
        m_render_window->clear();

        if (!m_error_display) {
            //draw all backgrounds, entities and texts
            draw_views(m_background);
            draw_views(m_entity_views);
            draw_views(m_text_views);

            //display useful info to the debugger
            if (m_debug_mode) {
                //show hitbox
                for (auto& entity_view: m_entity_views) {
                    entity_view->draw_hitbox(sf::Color::Green);
                }

                for (auto& bg_view: m_background) {
                    bg_view->draw_hitbox(sf::Color::Red);
                }

                //show entity count
                std::string text = " Entity count: " + std::to_string(m_entity_views.size());
                m_entity_count.setString(text);
                m_render_window->draw(m_entity_count);
            }
        } else {
            m_render_window->draw(m_error_message);
        }

        m_render_window->display();
    }

    void View::draw_views(std::list<std::shared_ptr<EntityView>>& views) {
        auto i = views.begin();

        //delete entity_views that are dead, else, draw them
        while (i != views.end()) {
            if ((*i)->get_status() == logic::dead) {
                (*i).reset();
                views.erase(i++);
            } else {
                (*i)->draw();
                ++i;
            }
        }
    }

    void View::switch_debug() {
        m_debug_mode = !m_debug_mode;
    }

    void View::flush(bool hard_flush) {
        m_background.clear();
        m_entity_views.clear();
        m_text_views.clear();
        if (hard_flush) m_textures.clear();
    }
    
    void View::display_error(const std::string& error_message) {
        flush(true);
        //divide the text over 2 lines so it fits on the screen
        string screen_fitted_message = error_message;
        if (error_message.size() > 30) {
            screen_fitted_message = error_message.substr(0, 30) + '\n' + error_message.substr(30, error_message.size() - 30);
        }
        //text is scaled to match the screen resolution
        m_error_message.setFont(m_default_font);
        m_error_message.setCharacterSize(15);
        m_error_message.setPosition(10 , 300);
        m_error_message.setColor(sf::Color::White);
        m_error_message.setString(screen_fitted_message);
        m_error_display = true;
    }

}

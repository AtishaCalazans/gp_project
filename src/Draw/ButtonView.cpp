//
// Created by atisha on 7/20/18.
//

#include "ButtonView.h"

namespace draw {

    ButtonView::ButtonView(const std::shared_ptr<logic::Button> &button, const std::shared_ptr<sf::RenderWindow> &window)
            : TextView(button, window) {
        m_button = button;
    }

    void ButtonView::update() {
        TextView::update();
        if (m_button->get_button_status() != m_button_status) {
            m_button_status = m_button->get_button_status();

            //create a border that fits around the text
            m_button_border.setSize(sf::Vector2f(m_text_sprite->getGlobalBounds().width + 5, m_text_sprite->getGlobalBounds().height + 5));
            m_button_border.setOrigin(m_text_sprite->getGlobalBounds().width/2 + 2.5, m_text_sprite->getGlobalBounds().height/2 + 2.5);
            m_button_border.setPosition(m_text_sprite->getPosition().x, m_text_sprite->getPosition().y - (m_text_sprite->getGlobalBounds().height/2));
            m_button_border.setOutlineColor(sf::Color::White);
            m_button_border.setOutlineThickness(3);

            //color the button depending on its status
            if (m_button_status == logic::rest) {
                m_button_border.setFillColor(sf::Color::Black);
            } else if (m_button_status == logic::hover) {
                m_button_border.setFillColor(sf::Color::Magenta);
            }
        }
    }

    void ButtonView::draw() {
        m_render_window->draw(m_button_border);
        TextView::draw();

    }

}
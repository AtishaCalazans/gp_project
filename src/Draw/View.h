//
// Created by atisha on 03.12.17.
//

#ifndef GP_PROJECT_VIEW_H
#define GP_PROJECT_VIEW_H

#include "BaseException.h"
#include <SFML/Graphics.hpp>
#include <map>
#include <memory>
#include <list>
#include <string>

namespace logic {
                class Entity;
                class Text;
                class Button;
                }

/**
 * @brief namespace that contains everything that has to do with visualization
 */
namespace draw {

    class EntityView;
    class TextView;
    class ButtonView;

    /**
     * @brief class that handles the communication with sfml
     */
    class View {
    public:

        View(const std::shared_ptr<sf::RenderWindow>& window);

        /**
         * @brief loads textures from Textures map
         * @param texture_names names of the texture files
         */
        void load_textures(std::vector<std::string>& texture_names);

        /**
         * @brief creates an EnitityView that is linked with an entity
         * @param entity the entity that is linked with the newly created EntityView
         */
        void add_entity(const std::shared_ptr<logic::Entity>& entity);

        /**
         * @brief creates a TextView that is linked with a Text object
         * @param text the text that is linked with the newly created TextView
         */
        void add_text(const std::shared_ptr<logic::Text>& text);

        /**
         * @brief add an entity that has to be drawn first on the screen (background, world)
         * @param entity the entity that is linked with the newly created EntityView
         * @param x_size the length of the sprite in the x direction
         * @param y_size the length of the sprite in the y direction
         */
        void add_background(const std::shared_ptr<logic::Entity>& entity, const int& x_size = 0, const int& y_size = 0);

        /**
         * @brief creates a ButtonView that is linked with a Button object
         * @param button the Button that is linked with the newly created TextView
         */
        void add_button(const std::shared_ptr<logic::Button>& button);

        /**
         * @brief draws all EntityViews and potential debug functionalities
         */
        void draw();

        /**
         * @brief deletes all EntityViews and textures
         * @param hard_flush true if textures have to be removed as well
         */
        void flush(bool hard_flush);

        /**
         * @brief switches debug mode on/off
         */
        void switch_debug();

        /**
         * @brief delete all EntityViews and display the error message
         */
        void display_error(const std::string& error_message);

    private:

        std::shared_ptr<sf::RenderWindow> m_render_window;
        std::string m_textures_loc;
        std::list<std::shared_ptr<EntityView>> m_background;
        std::list<std::shared_ptr<EntityView>> m_entity_views;
        std::list<std::shared_ptr<EntityView>> m_text_views;
        std::map<std::string, sf::Texture> m_textures;
        bool m_debug_mode = false;
        sf::Font m_default_font;
        sf::Text m_entity_count;
        sf::Text m_error_message;
        bool m_error_display = false;

         /**
         * @brief helper function for draw()
         */
        void draw_views(std::list<std::shared_ptr<EntityView>>& views);

    };

}

#endif //GP_PROJECT_VIEW_H

//
// Created by atisha on 7/20/18.
//

#ifndef GP_PROJECT_BUTTONVIEW_H
#define GP_PROJECT_BUTTONVIEW_H

#include "TextView.h"
#include "Button.h"

namespace draw {

    /**
     * @brief class that is associated with a Button entity, inherits from TextView
     */
    class ButtonView: public TextView {
    public:

        ButtonView(const std::shared_ptr<logic::Button> &button, const std::shared_ptr<sf::RenderWindow> &window);

        /**
         * looks up the position, rotation and status of the button
         */
        void update();

        /**
         * draws the sprite to the render window
         */
        void draw();

    private:

        std::shared_ptr<logic::Button> m_button;
        sf::RectangleShape m_button_border;
        logic::ButtonStatus m_button_status = logic::pressed;

    };

}

#endif //GP_PROJECT_BUTTONVIEW_H
